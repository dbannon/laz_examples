unit Unit1;

{$mode objfpc}{$H+}

{ An Example project to demonstrate use of the ListViewFilterEdit
component, a simple means to filter ListView contents.



Content to be displayed in the ListView must be loaded via the ListViewFilterEdit
control. Although not demonstrated here, its possible to include an object with
each row, much the same as can be done with ListView.


There does not appear to be a way to define an external search method. Therefore
you can search only the content actually loaded into the TListViewFilterEdit.

Do not use a ListViewFilterEdit with a ListView in Owner Data mode.

FilterOptions property should be used to make filtering case sensitive or not.

Following changes were made in the Object Inspector -
    ListView1.ViewStyle := vsReport;
    ListViewFilterEdit1.FilteredListview := ListView1;
    ListViewFilterEdit1.TextHint := 'Search Here';
    ListViewFilterEdit1.CharCase := ecNormal;
    Create two columns in the ListView, set first one to be autowidth


David Bannon, 2022-12-07

}
interface

uses
    Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ComCtrls,
    ListViewFilterEdit, editbtn;

type

    { TForm1 }

    TForm1 = class(TForm)
        Button1: TButton;
        CheckBothColumns: TCheckBox;
        CheckCaseSensitive: TCheckBox;
        Label1: TLabel;
        ListView1: TListView;
        ListViewFilterEdit1: TListViewFilterEdit;
        procedure Button1Click(Sender: TObject);
        procedure CheckBothColumnsChange(Sender: TObject);
        procedure CheckCaseSensitiveChange(Sender: TObject);
        procedure FormCreate(Sender: TObject);
    private
        procedure AddLVItem(St1, St2: string);
        procedure LoadSomeData();

    public

    end;

var
    Form1: TForm1;

implementation

{$R *.lfm}



procedure TForm1.AddLVItem(St1, St2 : string);
var
    ListItem: TListViewDataItem;
begin
    ListItem.Data := Nil;
    SetLength(ListItem.StringArray, 2);
    ListItem.StringArray[0] := St1;
    ListItem.StringArray[1] := St2;
    {%H-}ListViewFilterEdit1.Items.Add(ListItem);
end;

procedure TForm1.LoadSomeData();
begin
    ListView1.BeginUpdate;
    AddLVItem('String1',  'String2');
    AddLVItem('String3',  'String4');
    AddLVItem('A String', 'another String');
    AddLVItem('x string', 'Extra String');
    ListViewFilterEdit1.InvalidateFilter;
    ListViewFilterEdit1.ResetFilter;
    ListViewFilterEdit1.Text:='';
    ListView1.EndUpdate;
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
    LoadSomeData();
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
    ListViewFilterEdit1.Filter := 'x';
    ListViewFilterEdit1.InvalidateFilter;     // Filter for strings containing x
end;

procedure TForm1.CheckBothColumnsChange(Sender: TObject);
begin
    ListViewFilterEdit1.ByAllFields := CheckBothColumns.Checked;
    ListViewFilterEdit1.InvalidateFilter;
end;

procedure TForm1.CheckCaseSensitiveChange(Sender: TObject);
begin
    if  CheckCaseSensitive.checked then
       ListViewFilterEdit1.FilterOptions :=
            ListViewFilterEdit1.FilterOptions + [fsoCaseSensitive]
    else
       ListViewFilterEdit1.FilterOptions :=
            ListViewFilterEdit1.FilterOptions - [fsoCaseSensitive];
    ListViewFilterEdit1.InvalidateFilter;
end;

end.

