# Lazarus Examples

## Progress Report
May 2022 - 204 Example Projects tested, described and indexed. This is all examples, demos, samples and tests reviewed, edited, moved and re-arranged as necessary.


Very few of the tests were deemed useful in this context, most of the old categories made it through. There are haoever, still 27 projects I have deemed "needs work".


Details of examples that have been left out of finial run are [here](https://gitlab.com/dbannon/lazarus/-/issues)
 
Even in the Published List, there are some projects that appear to have minor issues. I have marked such projects with "Needs Work", they are useful as is but could be improved.



**Ref -** 

This 'project' developed from a forum discussion -

https://forum.lazarus.freepascal.org/index.php/topic,57680.0.html

The Wiki page has current state of play -

   https://wiki.freepascal.org/Lazarus_Examples_Window


Also relevent is this bug report -

https://gitlab.com/freepascal.org/lazarus/lazarus/-/issues/37509

and this forum thread -

https://forum.lazarus.freepascal.org/index.php/topic,57645.0.html



