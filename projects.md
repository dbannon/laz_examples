## memo
**Category** : Beginner

**Keywords** : TMemo 

[Beginner/memo](Beginner/memo)

**Description** : Shows how to add and copy text to and from TMemos.

## listview
**Category** : Beginner

**Keywords** : TListView SubItems 

[Beginner/listview](Beginner/listview)

**Description** : Demonstrates TListView, how to add and remove items and sub items.

## loadpicture
**Category** : Beginner

**Keywords** : TImage LoadFromFile 

[Beginner/loadpicture](Beginner/loadpicture)

**Description** : Shows how to load an image file and display it. In this case a png file but many other formats also supported.

## Laz_Hello
**Category** : Beginner

**Keywords** : Lazarus Hello World TButton TMemo 

[Beginner/Laz_Hello](Beginner/Laz_Hello)

**Description** : This might be your first Lazarus project, its the traditional Hello World.  Two buttons have been dropped on the form, renamed and their captions set. A memo was then added and each button was double clicked and what they are expected to do was set.

## Printer_dialogs
**Category** : Beginner

**Keywords** : Printer dialogs settings 

[Beginner/Printer_dialogs](Beginner/Printer_dialogs)

**Description** : Demonstrates how to display various printer dialogs and how to get printer data.

Appears to be a issue in this project where you need to (manually) build before clicking run. 



## TAChart_basic
**Category** : Beginner

**Keywords** : TAChart basic 

[Beginner/TAChart_basic](Beginner/TAChart_basic)

**Description** : A good introduction to using TAChart, will draw a number of different charts (based on random data) in just over 300 lines of code.

There are more TAChart examples under the TAChart category.

## CustomHint
**Category** : Beginner

**Keywords** : Hint Timeout Format TTrackBar 

[Beginner/CustomHint](Beginner/CustomHint)

**Description** : Shows how to control the shown Hint, Timeout etc. Demonstrates performing a function in the Hint (showing updated time of day). Also incidentally demonstrates TTrackBar.

## notebook
**Category** : Beginner

**Keywords** : TNoteBook 

[Beginner/notebook](Beginner/notebook)

**Description** : Demonstrates TNoteBook, a multipage GUI element.

## combobox
**Category** : Beginner

**Keywords** : TComboBox 

[Beginner/combobox](Beginner/combobox)

**Description** : Demonstrates TComboBox, allows user to select an entry from a pull down list.

## Laz_Scalable
**Category** : Beginner

**Keywords** : Anchors scaleable 

[Beginner/Laz_Scalable](Beginner/Laz_Scalable)

**Description** : This project has the anchors set of all its components in the Object Inspector. Now, as you resize the window, the components auto adjust their position.

## lclversion
**Category** : Beginner

**Keywords** : LCL Version Version String 

[Beginner/lclversion](Beginner/lclversion)

**Description** : Small app that demonstrates how to read the LCL version defined in every Lazarus app.
Also shows a couple of other useful strings, CPU, OS etc and how to compare LCL versions.

## messagedialogs
**Category** : Beginner

**Keywords** : MessageDlg ShowMessage 

[Beginner/messagedialogs](Beginner/messagedialogs)

**Description** : Shows how to display both ShowMessage and MessageDlg in a couple of formats.

## ControlHint
**Category** : Beginner

**Keywords** : Hint Controls Needs Work 

[Beginner/ControlHint](Beginner/ControlHint)

**Description** : Demonstrates a range of techniques relating to control hints.

Not clear to me what this is showing, maybe someone else can clarify ?

## comdialogs
**Category** : Beginner

**Keywords** : Common Dialogs TOpenDialog TSaveDialog TFontDialog TColorDialog TSelectDirectoryDialog 

[Beginner/comdialogs](Beginner/comdialogs)

**Description** : Shows a number of the commong dialog boxes. 

## listbox
**Category** : Beginner

**Keywords** : TListBox 

[Beginner/listbox](Beginner/listbox)

**Description** : Demonstrate TListBox, adding and removing entries.

## bitbtn
**Category** : Beginner

**Keywords** : TBitBtn 

[Beginner/bitbtn](Beginner/bitbtn)

**Description** : Demonstrates TBitBtn, a button with a bitmap image.

## Icons
**Category** : Beginner

**Keywords** : Icon ico icns Application Icon 

[Beginner/Icons](Beginner/Icons)

**Description** : Demonstrates loading icon files (ico and icns), scalling and setting as application icon.

## LazReport_userds
**Category** : Beginner

**Keywords** : LazReport userds 

[Beginner/LazReport_userds](Beginner/LazReport_userds)

**Description** : This sample demonstrates several LazReport features: how to build a custom Crosstab report using TfrUserDatasets only, using format script to control color and text content of cell report, and also how to use runtime events to modify report item look. See in app notes !

It requires the package LazReport be installed, from the Main Menu, 'Packages'->'Install/Uninstall Packages', enter 'LazReport' on RHS, select it in the list and click "Install Selection", 'Save and Rebuild IDE', 'Continue'. 

There are more LazReport examples under the LazReport category.



## paradox_demo
**Category** : DBase

**Keywords** : paradox dbase db 

[DBase/paradox_demo](DBase/paradox_demo)

**Description** : Appears to demonstrate basic use of Paradox Database.
Requires the lazparadox package (in Laz SRC Tree) to be installed.
Requires Paradox library on your system, eg on Debian pxlib1.

There is an as yet unexplained issue with this demo, execute "Clean up and Build" if an error "unable to find TDatasoruce" is displayed when loading the project.


## bufclient
**Category** : DBase

**Keywords** : sqldbrest REST TFPhttpclient TBufDataSet 

[DBase/bufclient](DBase/bufclient)

**Description** : One part of a set of demos (search for sqldbrest) demonstrating using REST to access a database. Compiles but not tested because it requires a server to connect to, possibly also provided in this set under RESTBridge ?

Requires the package weblaz be installed before loading.

Better documentation would be great !

## dbeditmask
**Category** : DBase

**Keywords** : TDBEdit TDBGrid Date 

[DBase/dbeditmask](DBase/dbeditmask)

**Description** : Demonstrates masking some DBEdits to control what can be entered into them.

## TSQLScript
**Category** : DBase

**Keywords** : DBase TSQLScript batch 

[DBase/TSQLScript](DBase/TSQLScript)

**Description** : This example shows how to use TSQLScript to run a batch of SQL statements.

Not well tested and may have issues in connecting to (at least sqlite3). Further research indicated.
Further info in the included readme.txt file.

## SQLite_Encryption_Pragma
**Category** : DBase

**Keywords** : DBase SQLite3 Encryption Pragma Needs Work 

[DBase/SQLite_Encryption_Pragma](DBase/SQLite_Encryption_Pragma)

**Description** : Simple SQLite3 Demo with Encryption and Pragma. Creation of an SQLite3 Database, Encrypting, Changing the key, Creating Tables, Index and adding rows, performing queries.

Is, however, broken on Unix systems because its assumes the sqlite3 library is called sqlite3.dll !  Leaving it here for now as it probably works under Windows but needs fixing!

See the included readme.txt file.


## restbridge
**Category** : DBase

**Keywords** : sqldbrest REST TFPhttpclient TBufDataSet Work Required 

[DBase/restbridge](DBase/restbridge)

**Description** : One part of a set of demos (search for sqldbrest) demonstrating using REST to access a database. Compiles  and runs (without any GUI). Might be associated with the other sqldbrest demos but listens on 8080 and they talk 3000. Needs clarification.

Better documentation would be great !

## Demo_CrossTab
**Category** : DBase

**Keywords** : Interbase Firebird crosstab 

[DBase/Demo_CrossTab](DBase/Demo_CrossTab)

**Description** : Builds but not tested.
Requires Interbase/Firebird to be installed before running.

## csvclient
**Category** : DBase

**Keywords** : sqldbrest REST TFPhttpclient TBufDataSet CVS 

[DBase/csvclient](DBase/csvclient)

**Description** : One part of a set of demos (search for sqldbrest) demonstrating using REST to access a database. Compiles but not tested because it requires a server to connect to, possibly also provided in this set under RESTBridge ?

Requires the package weblaz be installed before loading.

Better documentation would be great !

## restmodule
**Category** : DBase

**Keywords** : sqldbrest REST 

[DBase/restmodule](DBase/restmodule)

**Description** : One part of a set of demos (search for sqldbrest) demonstrating using REST to access a database. Compiles but not tested because it requires a server to connect to, possibly also provided in this set under RESTBridge ?

No GUI, just registers 'REST'.

Better documentation would be great !

## dBLookup
**Category** : DBase

**Keywords** : dbase Tdbf TBataSource 

[DBase/dBLookup](DBase/dBLookup)

**Description** : Demonstrates some basic database functions using Tdbf and TDataSource and its own dbase files.

## mssql
**Category** : DBase

**Keywords** : MSSQL Microsoft SQL Server SyBase Connector Needs Work 

[DBase/mssql](DBase/mssql)

**Description** : Example program for the MS SQL Server and Sybase connectors in Lazarus.

These connectors require the FreeTDS shared library (dblib.dll/.so/.dylib), which at least on Windows requires libiconv2.dll for UTF8 support.
These can be downloaded via www.freetds.org and are provided by a lot of Linux distributions.

There is a readme.txt file that should be consulted as you look at this example.

The readme.txt refers to ftp://ftp.freepascal.org/fpc/contrib/windows/ however that does not appear to work.



## stdexports
**Category** : DBase

**Keywords** : dbase database export Needs work 

[DBase/stdexports](DBase/stdexports)

**Description** : Demonstrates exporting a small database in a number of formats. Accepts an input file of your choice s may even be a useful utility.
See also dbexport_demo.
Please see the associated readme.txt file.
Appears to have some UTF8 issues, char may not be displayed correctly and some Pango errors reported to console when opening its own test file.

## dbexport_demo
**Category** : DBase

**Keywords** : dbase database export 

[DBase/dbexport_demo](DBase/dbexport_demo)

**Description** : Demonstrates exporting a small database in a number of formats. Accepts an input file of your choice so may even be a useful utility.
Requires the dbflaz package (part of bigide).
Requires the package LazDBExport be installed.
See also stdexports.



## SQLdBTutorial3
**Category** : DBase

**Keywords** : DBase SQLIte3 Tutorial wiki 

[DBase/SQLdBTutorial3](DBase/SQLdBTutorial3)

**Description** : This directory has the accompanying code for -
http://wiki.lazarus.freepascal.org/SQLdb_Tutorial3
Does not seem to connect flawlessly to sqlite3 so some reading of the wiki article might be a good idea. 

Please see that article for instructions and requirements.


## FBAdmin
**Category** : DBase

**Keywords** : DBase FBAdmin network 

[DBase/FBAdmin](DBase/FBAdmin)

**Description** : Uses FBAdmin to manage or admin a remote (Interbase?) dabatase.

## jsonclient
**Category** : DBase

**Keywords** : sqldbrest REST TFPhttpclient TBufDataSet json 

[DBase/jsonclient](DBase/jsonclient)

**Description** : One part of a set of demos (search for sqldbrest) demonstrating using REST to access a database. Compiles but not tested because it requires a server to connect to, possibly also provided in this set under RESTBridge ?

Requires the package weblaz be installed before loading.

Better documentation would be great !

## Address_book
**Category** : DBase

**Keywords** : DBGrids DBCtrls 

[DBase/Address_book](DBase/Address_book)

**Description** : Demonstrates making a simple database based Address Book.
Open the demo datafile from the 'File' menu to see it work.

## BiDi
**Category** : General

**Keywords** : Left to Right Right to Left 

[General/BiDi](General/BiDi)

**Description** : Demonstrates different BiDi Modes. Allows RTL languages such as Arabic and Hebrew to be used. In left-to-right, LTR, text entry starts at the left, text is almost left justified, and vertical scrollbars sit at the right side of the control. In right-to-left mode text entry starts at the right, text is almost right justified, and vertical scrollbars sit at the left side of the control. 

## mousebuttons
**Category** : General

**Keywords** : mouse buttons mouse wheel extra buttons 

[General/mousebuttons](General/mousebuttons)

**Description** : Demonstrates detecting the various mouse action including those of the more complicated multiple button mice.



## DragImageList
**Category** : General

**Keywords** : drag and drop drag Images Needs Work 

[General/DragImageList](General/DragImageList)

**Description** : This example demonstrates the usage of DragImageList. Or perhaps Drag and Drop.

You will see Button image cursor while drag.  This Button image is from the DragImageList. You can place any other image there to show it while drag operation.

Should the cursor change when dragged image is over a valid target ?

## ExploreMenu
**Category** : General

**Keywords** : IDE Register Menu Item 

[General/ExploreMenu](General/ExploreMenu)

**Description** : Note : Not Tested.

This example is a design time package. When installed it registers a new menu item in the IDE, which shows all the IDE menus as treeview.

## IDEQuickFix
**Category** : General

**Keywords** : IDE Package 

[General/IDEQuickFix](General/IDEQuickFix)

**Description** : This is an example of how to make an IDE Package. Its not a project you can run, is a package you install.

## translation
**Category** : General

**Keywords** : Translations POEdit i18n Needs Work 

[General/translation](General/translation)

**Description** : Demonstrates internationalisation using po file. 

Note - appears to use a more complicated mechanism (LocalizedForms.*) than is now necessary, needs review.

## CodePageConverter
**Category** : General

**Keywords** : DefaultTranslator Code Page TButton TMemo 

[General/CodePageConverter](General/CodePageConverter)

**Description** : Makes an app that will convert files between code pages.

## motiongraphics
**Category** : General

**Keywords** : Graphics Polygon Rotate Speed Drawing 

[General/motiongraphics](General/motiongraphics)

**Description** : Shows you how to rotate a polygon and vary the speed of rotation.

## pen_brush
**Category** : General

**Keywords** : Drawing styles lines 

[General/pen_brush](General/pen_brush)

**Description** : Demonstrates drawing (lines) in a range of styles, colours, widths etc.

## xmlstreaming
**Category** : General

**Keywords** : xml xml streaming components Needs work 

[General/xmlstreaming](General/xmlstreaming)

**Description** : Writes a component to an XML stream then uses that stream to create more such components. 

I am not sure the rewritten components popup where they should, that is in the Destination box ?

## lrspreadsheetexport_demo
**Category** : General

**Keywords** : lazreport spreadsheet export 

[General/lrspreadsheetexport_demo](General/lrspreadsheetexport_demo)

**Description** : Demonstrates creating and exporting a spreadsheet in several formats, ODS, XLS, XLSX

Depends on fpspreadsheet in OPM, install FIRST !
Depends on lrSpreadSheetExport.



## lazintfimage
**Category** : General

**Keywords** : Images fade in rotate Needs Work 

[General/lazintfimage](General/lazintfimage)

**Description** : Proports to show how to display an image with a fade in. However, the image, on my system, seems to appear instantly. Maybe a case of today's hardware being faster ?

Also demonstrates how to rotate that image to a particular angle.

## ImageList_HighDPI_DesignTime
**Category** : General

**Keywords** : TImageList TToolBar HighDPI 

[General/ImageList_HighDPI_DesignTime](General/ImageList_HighDPI_DesignTime)

**Description** : Demonstrates using an ImageList to populate a TToolBar, TTreeView, TListView and  TPageControl. Including use with HighDPI. 

## listview_example
**Category** : General

**Keywords** : listview Needs Work 

[General/listview_example](General/listview_example)

**Description** : Demonstrates some of the attributes of listview. Could perhaps do a bit more.

## widestringstreaming
**Category** : General

**Keywords** : Stream Components 

[General/widestringstreaming](General/widestringstreaming)

**Description** : This example demonstrates, how to stream a component to a stream in binary format and then recreate that component elsewhere from the stream.

## multithread_basic
**Category** : General

**Keywords** : Multithreading 

[General/multithread_basic](General/multithread_basic)

**Description** : Basic Multithreading, just starts an app that immediately runs 
one extra thread and updates the main form's caption. Close the form to stop it.


## Imagelist_HighDPI_RunTime
**Category** : General

**Keywords** : TImageList Run Time 

[General/Imagelist_HighDPI_RunTime](General/Imagelist_HighDPI_RunTime)

**Description** : Demonstrates how to use a TImageList at Run Time to put images on TPageControl, TToolBar, TListView, TMainMenu.


## HelpHTML
**Category** : General

**Keywords** : Help HTML Browser 

[General/HelpHTML](General/HelpHTML)

**Description** : Shows how to open the Users default browser and display some help files of your choice.

## idehelp
**Category** : General

**Keywords** : IDE Help 

[General/idehelp](General/idehelp)

**Description** : Demo package to show the various help types of the IDE.
Note, its a package that needs to be installed in the IDE, it is not a project you can just open. Written for a very early version of the IDE and may not be useful with something current.

## jpeg_more
**Category** : General

**Keywords** : Images jpeg jpg Scaling 

[General/jpeg_more](General/jpeg_more)

**Description** : Loads, displays and scales a JPEG file. 

Note : when altering scaling, press the "Read" button. I added its own copy of the cheetah image. DRB


## lazfreetype
**Category** : General

**Keywords** : Needs Work Fonts 

[General/lazfreetype](General/lazfreetype)

**Description** : Appears to want to show nominated fonts on screen.

However, defaults to Windows font names and does not seem to accept known locally available fonts on the command line as it claims it does.

Probably fair to say it needs some work on its cross platform abilities and better error messages when something unexpected happens.

## designnonlcl
**Category** : General

**Keywords** : Design non LCL Package 

[General/designnonlcl](General/designnonlcl)

**Description** : This package is an example how to to use the lazarus IDE form designer to design non LCL widgets like web pages, UML diagrams or other widgetsets like fpgui.

Its a Package that must be installed, not a project.

## messages
**Category** : General

**Keywords** : PostMessage SendMessage QueueSsyncCall 

[General/messages](General/messages)

**Description** : These two example demonstrates the usage of PostMessage, SendMessage and the message methods in Lazarus and FreePascal and its widgetset independent counterpart, the direct method call and QueueAsyncCall.

There is a readme.txt file that explains the process that you should read when looking at this example.




## openglcontrol
**Category** : General

**Keywords** : OpenGL Effects Rotate TOpenGLControl Drawing No Object Inspector 

[General/openglcontrol](General/openglcontrol)

**Description** : Demonstrates a number of effects using the TOpenGLControl.

Note, written before Lazarus had an ObjectInspector so components are created 'on the fly' in user code. You don't need to do that now.

## std_actions
**Category** : General

**Keywords** : TToolBar TActionList TToolButton 

[General/std_actions](General/std_actions)

**Description** : Demonstrates how a TToolBar can work with a TActionList to implement the standard editing actions (Copy, Paste, delete, find etc).

## CoolToolBar
**Category** : General

**Keywords** : TCoolBar TToolBar TActionList TAction IniFile 

[General/CoolToolBar](General/CoolToolBar)

**Description** : Demonstrates TCoolBar and TToolBar in a number of modes. Also uses TAction and TActionList. Saves and loads the coolbar to and from an ini file.

## htmlhelp_ipro
**Category** : General

**Keywords** : Help iPro Browser 

[General/htmlhelp_ipro](General/htmlhelp_ipro)

**Description** : This example demonstrates the html help components using the turbo power ipro browser component to show local html files.             

## multithread_single
**Category** : General

**Keywords** : Multithreading 

[General/multithread_single](General/multithread_single)

**Description** : Demo to show, how to process a big file with the main thread while keeping the application responsive. 


## multithread_wait
**Category** : General

**Keywords** : Multithreading 

[General/multithread_wait](General/multithread_wait)

**Description** : Demo to show, how a Thread waits for another. 


## multithread_critical
**Category** : General

**Keywords** : Multithreading 

[General/multithread_critical](General/multithread_critical)

**Description** : Demonstrates how to use a CriticalSection in a multithreaded application and, incidentally, how important it might be ! 





## DropFiles
**Category** : General

**Keywords** : drag files drag and drop 

[General/DropFiles](General/DropFiles)

**Description** : Demonstrates dropping a file dragged from a file manager and displaying the result. 

## openurltest
**Category** : General

**Keywords** : Open Browser Open application Open Document 

[General/openurltest](General/openurltest)

**Description** : Demonstrates how to open either your system's default browser or the system's default for a local document.

## vst_advanced
**Category** : General

**Keywords** : TLazVirtualStringTree TVirtualTreeView Advanced Features 

[General/vst_advanced](General/vst_advanced)

**Description** : This is about TLazVirtualStringTree, a component renamed from TVirtualTreeView that exists in the LazControls Component Tab. 

This example demonstrates some advanced features such as Extended Characters, Property Page, Syncing Trees, Grid Extensions, A Draw Tree (showing images), bidi ability,  Painting the tree under a specific theme (ie WinXP style), Spanning Columns and Advanced drawing of Headers.

## objectinspector
**Category** : General

**Keywords** : ObjectInspector TOIPropertyGrid IDE Components 

[General/objectinspector](General/objectinspector)

**Description** : Demonstrates how the ObjectInspector works, uses TOIPropertyGrid to display the attributes of its own form in an ObjectInspector mode. 

## vst_dragdrop
**Category** : General

**Keywords** : TLazVirtualStringTree TVirtualTreeView Drag and Drop 

[General/vst_dragdrop](General/vst_dragdrop)

**Description** : This is about TLazVirtualStringTree, a component renamed from TVirtualTreeView that exists in the LazControls Component Tab. 

This example shows drag and drop (from a TListBox) to the TLazVirtualStringTree.

## shapedcontrols
**Category** : General

**Keywords** : Shaped No rectangular 

[General/shapedcontrols](General/shapedcontrols)

**Description** : How to make controls, in this case a Button, a different shape than the traditional rectangle.

## vst_minimal
**Category** : General

**Keywords** : TLazVirtualStringTree TVirtualTreeView 

[General/vst_minimal](General/vst_minimal)

**Description** : This is about TLazVirtualStringTree, a component renamed from TVirtualTreeView that exists in the LazControls Component Tab. 

This example shows it most basic use.

## mouseandkeyinput_example
**Category** : General

**Keywords** : mouse keyboard input simulate 

[General/mouseandkeyinput_example](General/mouseandkeyinput_example)

**Description** : Shows how to simulate mouse and keyboard input programmatically, for example triggered by the press of a button.

## propstorage
**Category** : General

**Keywords** : TIniPropStorage TXMLPropStorage config file configuration file 

[General/propstorage](General/propstorage)

**Description** : Records the size, location and contents (ie the Properties) of a  TMemo in both an xml and ini file for later retrival.

## turbopower_ipro
**Category** : General

**Keywords** : TurboPower html renderer 

[General/turbopower_ipro](General/turbopower_ipro)

**Description** : This directory contains two programs to test the package 
TurboPower Internet Professional. 
1. tpiproexample: Basic function to display and print a html page.
2. tpiprohtmltree: Displays the hierarchy of html nodes of a html document   in a treeview.

## LCLReport
**Category** : General

**Keywords** : Report PDF fpReport 

[General/LCLReport](General/LCLReport)

**Description** : Demonstrates generating a report (from some dummy data), displaying it and converting it to a PDF.

If the program did not compile or run, look if you have the correct freetype-6.dll/.so and zlib1.dll/.so installed or copied in the directory.
AF 2018

## treeview
**Category** : General

**Keywords** : TTreeView 

[General/treeview](General/treeview)

**Description** : Demonstrates adding/removing nodes to/from a TTreeView.
See https://web.archive.org/web/20180423180227/http://users.iafrica.com/d/da/dart/Delphi/TTreeView/TreeView.html

## NoteBk
**Category** : General

**Keywords** : tabbed notebook No Object Inspector 

[General/NoteBk](General/NoteBk)

**Description** : This project was built without using the Lazarus IDE's Form designer because either the author preferred working that way or it was done before the Designer was available. It demonstrates how components can be placed on different pages of a notebook.

## stock_images
**Category** : General

**Keywords** : Images Dialog Images Button Images TTrackBar 

[General/stock_images](General/stock_images)

**Description** : Displayes, interactivly, the "stock Images" that Lazarus uses in Dialogs and Buttons.

## GroupedControl
**Category** : General

**Keywords** : RTTI RTTIGrids TTIPropertyGrid TGroupBox 

[General/GroupedControl](General/GroupedControl)

**Description** : Compares the use of a TGroupBox with some other techniques using RTTI components TTIPropertyGroup to group controls. Also introduces a self defined TCustomTransparentPanel and TCustomGroupedEditButton. 

## Canvas_Test
**Category** : General

**Keywords** : Drawing Canvas Needs Work 

[General/Canvas_Test](General/Canvas_Test)

**Description** : Demonstrates drawing directly on the canvas in a number of styles. Noted a crash when drawing Shaped Window.

## FontEnum
**Category** : General

**Keywords** : fonts monospace character set 

[General/FontEnum](General/FontEnum)

**Description** : Application that demonstrates reading the existing fonts on your system and displaying styles and character sets. Can filter by, eg Monospace. 

## CleanDir
**Category** : General

**Keywords** : Needs Work daemon service application 

[General/CleanDir](General/CleanDir)

**Description** : This is the cleandirs demonstration program for Lazarus.

Note - problem loading cleandirs that appears to relate to a redefinition of streams.

It demonstrates 3 things:

1. How to use services and the TEventLog component (cleandirs)
2. How to use RTTI controls (confcleandirs)
3. How to write a TCustomApplication descendent (cleandir)

See the included README.TXT for much more information.



## levelgraph
**Category** : General

**Keywords** : TLvlGraphControl TTIPropertyGrid 

[General/levelgraph](General/levelgraph)

**Description** : Demonstrates use of TLvlGraphControl (LazControls tab) to display a level graph, used to show the relationship between a number of interrelated entities.


## ImgViewer
**Category** : General

**Keywords** : Images TImage scaling FindFirstUTF8 BeginWaitCursor 

[General/ImgViewer](General/ImgViewer)

**Description** : This example application shows how to load and show image/graphical files.

It also demonstrates
- scaling images
- using the Lazarus functions FindFirstUTF8 and FindNextUTF8 to recursively seek files and directories
- dealing with key presses using the KeyDown event
- using BeginUpdate and EndUpdate to improve processing speed of certain controls (a ListBox in this case)
- setting the cursor to hourglass and reset it to indicate a long-running operation is going on (e.g. when recursively loading directories with a large amount of images)

## ComponentStreaming
**Category** : General

**Keywords** : Stream Component 

[General/ComponentStreaming](General/ComponentStreaming)

**Description** : This example demonstrates how to stream a component to a stream in binary format and how to reconstruct the component from a stream. This technique can be used to save components to disk or to transfer them via network. Of course this also works for your own classes as long as they are descendants of TComponent.

## designerbaseclass
**Category** : General

**Keywords** : Base Class LCL Package 

[General/designerbaseclass](General/designerbaseclass)

**Description** : demonstrates how you might provide a new Base Class. The demo requires  you install a package with that base class and the run a demo project. The demo project will not load if the Package has not been installed first.


## lazresexplorer
**Category** : General

**Keywords** : resources 

[General/lazresexplorer](General/lazresexplorer)

**Description** : Shows how to display the resources in a lazarus resource file or a binary, DLL or object file.
The actual content may not be particularly useful as it is but the techniques used do demonstrate how to read the mostly bitmap resources in these files might be.



## SynEdit - Search And Replace
**Category** : General

**Keywords** : TSynEdit Search Replace SearchAndReplace 

[General/SynEdit - Search And Replace](General/SynEdit - Search And Replace)

**Description** : Demonstrates TSynEdit's Search and Replace capability, and the range of options it can use.

## popupnotifier
**Category** : General

**Keywords** : TPopupNotifier message 

[General/popupnotifier](General/popupnotifier)

**Description** : Shows how to use TPopupNotifier, pops up a small yellow (?) window with a message for the user.

(You may, as an alternative use Libnotify on Linux, other more OS friendy notification tools exist on other OSs, just a suggestion)



## SynEdit - SynAny Highlighter
**Category** : General

**Keywords** : TSynEdit runtime 

[General/SynEdit - SynAny Highlighter](General/SynEdit - SynAny Highlighter)

**Description** : TSynAnySyn allows one to highlight specific words in the text.
It can be used to highlight every occurrence of 'Lazarus' in a specific color.
This example shows, how the the Keywords, Objects and Constants properties of the class TSynAnySyn can be controlled at runtime.  Text can be highlighted following a number of rules.



## SynEdit - Completion
**Category** : General

**Keywords** : TSynEdit TSynCompletion TSynAutoComplete Text Completion Word List 

[General/SynEdit - Completion](General/SynEdit - Completion)

**Description** : SynEdit has 2 "auto-completion" features.

1) TSynCompletion (The word completion feature used by the IDE)
Displays a drop-down providing a list of completions (or replacements) for the current word. The completions, must be provided, and updated by the application.
If the user typed "Sel", then a list of words beginning with "Sel" may be displayed.

2) TSynAutoComplete
Replaces the current word/token, with a fixed pre-defined substitution.
If the user typed "IB", the replacement may be "if foo then begin"




## SynEdit - New Highlighter Tutorial
**Category** : General

**Keywords** : TSynEdit wiki highlighter TSynHighlighterAttributes 

[General/SynEdit - New Highlighter Tutorial](General/SynEdit - New Highlighter Tutorial)

**Description** : Example for the "Create your own SynEdit Highlighter" tutorial on the wiki http://wiki.lazarus.freepascal.org/SynEdit_Highlighter

## ChildSizingLayout
**Category** : General

**Keywords** : Layout Sizing ChildSizing 

[General/ChildSizingLayout](General/ChildSizingLayout)

**Description** : This is a demonstration of the TWinControl.ChildSizing.Layout property.

## Easter
**Category** : General

**Keywords** : Dates Easter year 

[General/Easter](General/Easter)

**Description** : Demonstrates handling dates and calculating years in advance. Small that that calculates Dates for a number of Christian Feast Days in a year.

## xmlreader
**Category** : General

**Keywords** : xml xml reader ReadXMLFile TTreeView 

[General/xmlreader](General/xmlreader)

**Description** : Shows how to read an XML file, displaying the data on its various nodes.

## jpeg
**Category** : General

**Keywords** : Images jpeg jpg TOpenPictureDialog TImage TJPEGImage 

[General/jpeg](General/jpeg)

**Description** : Opens and saves a JPEG image. Note, the SRC has a mention of the need to open the JPEG page first but that appears no longer needed. Further it mentions a README.txt but that is apparently no longer here.

## postscript
**Category** : General

**Keywords** : Postscript png xpm bmp Drawing 

[General/postscript](General/postscript)

**Description** : How to create a Postscript file, displaying images, text and polygons on it.
(Note that some functions don't work unless you have specific tools installed but creating the PS file requires nothing but Lazarus.)

## SynEdit - SynPosition Highlighter
**Category** : General

**Keywords** : TSynEdit TSynPositionHighlighter 

[General/SynEdit - SynPosition Highlighter](General/SynEdit - SynPosition Highlighter)

**Description** : Example for SynEdit's Highlighter: TSynPositionHighlighter. SynEdit is the editor used by the IDE.

TSynPositionHighlighter allows one to highlight text at one or more fixed position(s). For example, it can be used to highlight the first 3 letters of the 2nd line of text, in a specific color.

## BitButton
**Category** : General

**Keywords** : TBitButton 

[General/BitButton](General/BitButton)

**Description** : This example shows how a BitButton, that is a button with a small bitmap, can be used.

## pascalstream
**Category** : General

**Keywords** : Needs Work component stream 

[General/pascalstream](General/pascalstream)

**Description** : Might be about writing a component to a stream. Seems to create a non-visual component and stream that. This description (and maybe the demo) needs to be improved.

## lpicustomdata
**Category** : General

**Keywords** : LPI File Custom Data Needs Work 

[General/lpicustomdata](General/lpicustomdata)

**Description** : This example shows how to read the custom data of an .lpi file and how to change it. The .lpi file is given as first command line parameter.

This example has not been tested because I simply don't understand what custom data belongs in an LPI file. If you do, please contribute.




## ListViewFilterEdit
**Category** : General

**Keywords** : List View Filter Search Here 

[General/ListViewFilterEdit](General/ListViewFilterEdit)

**Description** : An Example project to demonstrate use of the ListViewFilterEdit component, a simple means to filter ListView contents.

See also https://wiki.freepascal.org/TListViewFilterEdit

## Cursors
**Category** : General

**Keywords** : Cursor crHourGlass ctSQLWait crNoDrop crDrag 

[General/Cursors](General/Cursors)

**Description** : Demonstrates changing and using Cursor to display various states. Includes Dragging cursors, Drop and NoDrop, HourGlass and custom cursors.

## Affinetransforms
**Category** : General

**Keywords** : Images bitmap transforms SetMapMode MM_ANISOTROPIC MM_ISOTROPIC MM_LOENGLISH MM_HIENGLISH MM_LOMETRIC MM_HIMETRIC MM_TWIPS MM_ANISOTROPIC 

[General/Affinetransforms](General/Affinetransforms)

**Description** : Demonstrates displaying a bitmap in a range of Map Modes.

## SynEdit - GutterMarks
**Category** : General

**Keywords** : TSynEdit Gutter Bookmarks 

[General/SynEdit - GutterMarks](General/SynEdit - GutterMarks)

**Description** : Example for SynEdit's Bookmarks, SynEdit is the editor used by the Lazarus IDE.

This example shows how to display Bookmarks and other marks/icons in the gutter of the editor.

## sprites
**Category** : General

**Keywords** : TPicture Sprite Images png 

[General/sprites](General/sprites)

**Description** : Shows how to move a small bitmap (a sprite) over a background image. Moves randomly or under control of the mouse or keys.

## TDaemon
**Category** : General

**Keywords** : TDaemom Server 

[General/TDaemon](General/TDaemon)

**Description** : These are supporting files that relate to the https://wiki.freepascal.org/Daemons_and_Services tutorial. While useful as is, reading the wiki article is strongly suggested. 
Requires that the package, LazDaemon be installed.

## JumpToImplementation
**Category** : General

**Keywords** : IDE Menu 

[General/JumpToImplementation](General/JumpToImplementation)

**Description** : Demonstrates how to add a new menu item to the IDE. Its not a project, see the individual files. This example adds the Jump To Implementation menu item.

## SpreadSheet
**Category** : General

**Keywords** : Grid TStringGrid SpreadSheet 

[General/SpreadSheet](General/SpreadSheet)

**Description** : Builds a basic spreadsheet.

## rotate_example
**Category** : General

**Keywords** : opengl graphics rotating cube 

[General/rotate_example](General/rotate_example)

**Description** : Demo, using OpenGL of the classic rotating colored cube. 

## AggPas_LCLDemo1_example
**Category** : General

**Keywords** : AggPas 2d vector Graphics AGG Textout 

[General/AggPas_LCLDemo1_example](General/AggPas_LCLDemo1_example)

**Description** : Draws and writes some text on a AggLCLCanvas.

AggPas is an Object Pascal port of the Anti-Grain Geometry library - AGG,  originally written by Maxim Shemanarev in industrially standard C++. 
 AGG as well as AggPas is Open Source and free of charge 2D vector graphic
 library.

For further information, see the readme.txt file in <LazDir>/components/aggpas.

## AggPas_LCLDemo2_example
**Category** : General

**Keywords** : AGG Canvas shapes Anti-Grain Geometry 2D Vector Graphics 

[General/AggPas_LCLDemo2_example](General/AggPas_LCLDemo2_example)

**Description** : Demonstrates writing text and a range of shapes to a canvas using AGG.

AggPas is an Object Pascal port of the Anti-Grain Geometry library - AGG,
 originally written by Maxim Shemanarev in industrially standard C++. 
 AGG as well as AggPas is Open Source and free of charge 2D vector graphic
 library.

For further information, see the readme.txt file in <LazDir>/components/aggpas.

## usergui_example
**Category** : General

**Keywords** : opengl OpenGlContext Controls 

[General/usergui_example](General/usergui_example)

**Description** : Includes a unit, OpenGLContext that has a number of controls build using OpenGL and demonstrates a basic (and quite retro looking) user interface, buttons, sliders etc.

## mrumenu_demo
**Category** : General

**Keywords** : MRU most recently used open 

[General/mrumenu_demo](General/mrumenu_demo)

**Description** : Appears to demonstrate a most recently used file open behaviour. Perhaps for Operating Systems that don't have that available in their normal file manager interface ?  

## plotfunction_expression
**Category** : General

**Keywords** : fpexprpars plot expression 

[General/plotfunction_expression](General/plotfunction_expression)

**Description** : Plots a square function with adjustable values.

## plotevent_demo
**Category** : General

**Keywords** : plotpanel event driven plotting data 

[General/plotevent_demo](General/plotevent_demo)

**Description** : Demonstrates plotting data obtained from a procedure. That procedure could be a Lazarus Event (but is not in the example).


## google_drive
**Category** : General

**Keywords** : Google API Synapse Google Drive Needs Work 

[General/google_drive](General/google_drive)

**Description** : Demonstrates access to the Google API, as long as Synapse is installed, compiles and starts up fine but not tested because I don't have a Google API Access Code. Its possible that Google no longer supports the OAuth mode that is used here as testing generates a Google Invalid Client rather than a signing request. And does not behave very well on a 'cancel'. 
Requires Synapse from OPM
Requires the use of a Google API Access Code

## google_gmail
**Category** : General

**Keywords** : Google API Synapse gmail Needs Work 

[General/google_gmail](General/google_gmail)

**Description** : Demonstrates access to the Google API, as long as Synapse is installed, compiles and starts up fine but not tested because I don't have a Google API Access Code. Its possible that Google no longer supports the OAuth mode that is used here as testing generates a Google Invalid Client rather than a signing request. And does not behave very well on a 'cancel'. 
Requires Synapse from OPM
Requires the use of a Google API Access Code

## cairocanvas_example
**Category** : General

**Keywords** : Cairo PDF Postscript svg png 

[General/cairocanvas_example](General/cairocanvas_example)

**Description** : Package for use libcairo in fpc. This example does NOT display a GUI, it writes out a number of files, PDF, PS, SVG and PNG the content of which are generated within the example.

This cairocanvas example require the Cairo Graphics and Pango libraries.

See the associated readme.txt file.



## Google_discovery_demo
**Category** : General

**Keywords** : Google API discovery Synapse 

[General/Google_discovery_demo](General/Google_discovery_demo)

**Description** : Finds and displays the Google API Services.
Requires the synapse package from the OPM.

## AggPasPango_example
**Category** : General

**Keywords** : Pango AGG textout Anti-Grain Geometry 2D Vector Graphics 

[General/AggPasPango_example](General/AggPasPango_example)

**Description** : Demonstrates using Pango to write text on a bitmap and display it in a Lazarus widget.

AggPas is an Object Pascal port of the Anti-Grain Geometry library - AGG,
 originally written by Maxim Shemanarev in industrially standard C++. 
 AGG as well as AggPas is Open Source and free of charge 2D vector graphic
 library.

For further information, see the readme.txt file in <LazDir>/components/aggpas.

## Google_tasks_demo
**Category** : General

**Keywords** : Google API Synapse Tasks Needs Work 

[General/Google_tasks_demo](General/Google_tasks_demo)

**Description** : Demonstrates access to the Google API, as long as Synapse is installed, compiles and starts up fine but not tested because I don't have a Google API Access Code. Its possible that Google no longer supports the OAuth mode that is used here as testing generates a Google Invalid Client rather than a signing request. And does not behave very well on a 'cancel'. 
Requires Synapse from OPM.
Requires the use of a Google API Access Code

## Google_calendar_demo
**Category** : General

**Keywords** : google api calendar Synapse Needs Work 

[General/Google_calendar_demo](General/Google_calendar_demo)

**Description** : Demonstrates access to the Google API, as long as Synapse is installed, compiles and starts up fine but not tested because I don't have a Google API Access Code. Its possible that Google no longer supports the OAuth mode that is used here as testing generates a Google Invalid Client rather than a signing request. And does not behave very well on a 'cancel'. 
Requires Synapse from OPM
Requires the use of a Google API Access Code

## Printer_rawmode
**Category** : General

**Keywords** : Printer raw dot matrix 

[General/Printer_rawmode](General/Printer_rawmode)

**Description** : Shows how to write to a dot matrix in raw mode. Probably not very useful (or safe) on the more modern page orientated printers. So, please take care !

## ReSizeImageDemo
**Category** : General

**Keywords** : Needs Work Resize FormResize 

[General/ReSizeImageDemo](General/ReSizeImageDemo)

**Description** :  LCL demo of redraw event order for images, panels and forms. it should work  for all widget sets eg Windows, qt, OSX, GTK etc.

Note: I cannot quite understand what this is demonstrating, maybe someone else can improve the metadata ?  DRB


## imagelist
**Category** : General

**Keywords** : Images ImageList Effects 

[General/imagelist](General/imagelist)

**Description** : Shows how to load an image and display it in several "effects" modes, greyed, hilite, shadowed and 1 bit.

## ColumnEditors
**Category** : General

**Keywords** : grid TStringGrid editor columns Wiki 

[General/ColumnEditors](General/ColumnEditors)

**Description** : Demonstrates a number of things relating to Column Editors, EditMask, changing colour of cells,  triggering events.

See the included readme.txt and wiki page http://wiki.freepascal.org/Grids_Reference_Page


## AndroidLCL
**Category** : General

**Keywords** : Android SQLite TButton TMemo Needs Work 

[General/AndroidLCL](General/AndroidLCL)

**Description** : Demonstrates and LCL based Android project. Two projects, one requires ARM (ie Android) cross compiler and other SQLite3. Needs a far better description than this.

## Merged_Cells
**Category** : General

**Keywords** : Grid Merge Cells 

[General/Merged_Cells](General/Merged_Cells)

**Description** : In this sample project you'll find a derived stringgrid which allows to combine adjacent cells to a larger block ("merge") easily.

See the included readme.txt for details on how to use it.




## Cell_OverFlow
**Category** : General

**Keywords** : Grid Cell Overflow TStringGrid 

[General/Cell_OverFlow](General/Cell_OverFlow)

**Description** : This sample project implements overflowing text in a StringGrid descendant. This means that text which is longer than the width of the column is not truncated at the cell border, but is allowed to flow into adjacent empty cells.

The code is based on a forum contribution by user Geepster 
(http://forum.lazarus.freepascal.org/index.php/topic,35869.msg238079.html#msg238079)

It was extended to correctly handle left, right and centered text alignments.

## title_images
**Category** : General

**Keywords** : Grid TStringGrid Sort Indicator 

[General/title_images](General/title_images)

**Description** : Demonstrates managing location and image of a String List's Sort Indicators.

## GridCellEditor
**Category** : General

**Keywords** : Grid TStringGrid Cell Editor 

[General/GridCellEditor](General/GridCellEditor)

**Description** : Demonstrates a simple Cell Editor model using a String Grid

## VLC_test
**Category** : General

**Keywords** : VLC video mp4 

[General/VLC_test](General/VLC_test)

**Description** : Makes a small app that uses the VLC libraries, to display video content.

Note that it generates a number of warnings to console and does not seem to be particularly happy working under the debugger in the IDE. 
Note that the Qt option is Qt4.



## embedded_images
**Category** : General

**Keywords** : Grid TStringGrid Image 

[General/embedded_images](General/embedded_images)

**Description** : Demonstrates how to embed an image in a StringGrid.

## TrayIcon
**Category** : General

**Keywords** : trayicon systray 

[General/TrayIcon](General/TrayIcon)

**Description** : This project demonstrates how to use the TrayIcon, it places a small Icon in the System Tray that the user can use to interact with the application through. 

## scanline
**Category** : General

**Keywords** : pf24bit ScanLine Images Delphi 

[General/scanline](General/scanline)

**Description** :     This example demonstrates how to
    - create an image with an internal format similar to Delphi's pf24bit
    - convert it to current format and create a TBitmap from it
    - use an approach similar to Delphi's TBitmap.ScanLine.
    
  Delphi's TBitmap implementation only supports windows formats. For example  the TBitmap.ScanLine function gives a direct pointer to the memory. This is not possible under all widget sets. And even those who supports it, uses different formats than windows. So Delphi code using TBitmap.ScanLine has to  be changed anyway. How much depends on how much speed is needed.
  
 If the goal is to quickly port some Delphi code using TBitmap.Scanline, then  the this code gives some hints how to achieve it.


## TAChart_lazreport
**Category** : LazReport

**Keywords** : TAChart LazReport 

[LazReport/TAChart_lazreport](LazReport/TAChart_lazreport)

**Description** : Load a LazReport into a TAChart.

Requires the package LazReport



## lr_codereport_sample
**Category** : LazReport

**Keywords** : LazReport CodeReport PDF 

[LazReport/lr_codereport_sample](LazReport/lr_codereport_sample)

**Description** : Generates a report and can export it as a PDF.
Requires LazReportPDFExport and lr_CodeReport
Requires  powerPDF from the OnLine Package Manager (OPM).

## Image_Mushrooms
**Category** : LazReport

**Keywords** : DBase SQLIte3 DBNavigator Images SQLQuery Firebird LazReport 

[LazReport/Image_Mushrooms](LazReport/Image_Mushrooms)

**Description** : Makes a small app that displays images of mushrooms and some text about each. Uses SQLite or Firebird, SqlDB, LazReport and FBAdmin. Demonstrates creating tables and running multiple SQLStatements.
Easy to get going and impressive demo.

See the included readme.txt file.

## lrFclPDFExport_demo
**Category** : LazReport

**Keywords** : LazReport PDF lrPDFExport lr_pdfexport 

[LazReport/lrFclPDFExport_demo](LazReport/lrFclPDFExport_demo)

**Description** : Demonstrates how to generate PDF reports from LazReport.
Requires the package lr_PDFExport be installed before loading.


## LazReport_stringgrid
**Category** : LazReport

**Keywords** : LazReport stringgrid basic 

[LazReport/LazReport_stringgrid](LazReport/LazReport_stringgrid)

**Description** : A very basic LazReport project, no database and the data is not saved. But does demonstrate basic use of LazReport.

Does require the LazReport package.



## Laz_report_url
**Category** : LazReport

**Keywords** : LazReport URL 

[LazReport/Laz_report_url](LazReport/Laz_report_url)

**Description** : Demonstrates a basic report that contains several URLs. The person reading the report can click a URL and its content is acted on by the report. 

Requires the LazReport package.


## test_lr_formstorage
**Category** : LazReport

**Keywords** : LazReport prompt input 

[LazReport/test_lr_formstorage](LazReport/test_lr_formstorage)

**Description** : Demonstrates prompting the end user for input that is used in a LazReport

Requires lr_DialogDesign package be installed.


## Laz_Report_ChildBand_Example
**Category** : LazReport

**Keywords** : LazReport child filter TfrBand 

[LazReport/Laz_Report_ChildBand_Example](LazReport/Laz_Report_ChildBand_Example)

**Description** : Demonstrates filtering LazReport content using the Band approach.

Requires the LazReport package be installed. 


## LazReport_console_cgi_http
**Category** : LazReport

**Keywords** : LazReport http server console powerpdf 

[LazReport/LazReport_console_cgi_http](LazReport/LazReport_console_cgi_http)

**Description** : Builds an http server and several versions of a client console app that  work with a database and LazReport. 

Requires the powerpdf package from On Line Package manager.
Note: Select a build mode (other than 'Default') before building.
Note: There is an extensive readme.txt that you almost certainly need to read.



## LazReport_editor
**Category** : LazReport

**Keywords** : LazReport editor printing 

[LazReport/LazReport_editor](LazReport/LazReport_editor)

**Description** : A quite expansive demonstration of LazReport including preparing the report in a number of views and printing.

Requires that LazReport, lr_tdbf,  lr_add_function and lr_extexp be installed before building.
Has an extensive readme.txt file.



## LazReport_sql_demo
**Category** : LazReport

**Keywords** : LazReport Interbase Firebird 

[LazReport/LazReport_sql_demo](LazReport/LazReport_sql_demo)

**Description** : Requires lr_tdbf, lr_SqlDB,  lr_dialogdesign packages to build.
Requires InterBase / Firebird client libraries to run.


## detail_reports
**Category** : LazReport

**Keywords** : LazReport Interbase 

[LazReport/detail_reports](LazReport/detail_reports)

**Description** : Demonstrates importing data from databases and speadsheets into a LazReport.

Requires lazReport, lr_OfficeImport,  lr_tdbf, lr_SqlDB packages.
Requires FPSpreadsheet from OPM (yes, including dcpcrypt).
Requires the Firebird libraries to run the Firebird part of demo but builds fine without.


## LazReport_dbf_demo
**Category** : LazReport

**Keywords** : LazReport DBf 

[LazReport/LazReport_dbf_demo](LazReport/LazReport_dbf_demo)

**Description** : Demonstrates a simple LazReport obtaining data from a DBf file.

Requires  LazReport, lr_tdbf ,  lr_dialogdesign package to build.

## TAChart_axisalign
**Category** : TAChart

**Keywords** : TAChart align axis 

[TAChart/TAChart_axisalign](TAChart/TAChart_axisalign)

**Description** : How to align the Axises of graphs in the same chart. 

## TAChart_area
**Category** : TAChart

**Keywords** : TAChart Area 

[TAChart/TAChart_area](TAChart/TAChart_area)

**Description** : Demonstrates a number of different modes of Area graphs.

## TAChart_clone
**Category** : TAChart

**Keywords** : TAChart clone duplicate 

[TAChart/TAChart_clone](TAChart/TAChart_clone)

**Description** : How to clone or duplicate the plot of a dataset. 

## TAChart_radial
**Category** : TAChart

**Keywords** : TAChart Pie explode 

[TAChart/TAChart_radial](TAChart/TAChart_radial)

**Description** : Demonstrate a pie chart where a click on a segment explodes it or moves it out.

## TAChart_liveview_paned
**Category** : TAChart

**Keywords** : TAChart liveview pane animated 

[TAChart/TAChart_liveview_paned](TAChart/TAChart_liveview_paned)

**Description** : Displays three panes, each with a chart that grows over time.

## TAChart_barseriestools
**Category** : TAChart

**Keywords** : TAChart bar 

[TAChart/TAChart_barseriestools](TAChart/TAChart_barseriestools)

**Description** : A demonstration of bar chart using TATools. Detecting clicks in the bars and responding. 

## TAChart_panes-2
**Category** : TAChart

**Keywords** : TAChart panes 

[TAChart/TAChart_panes-2](TAChart/TAChart_panes-2)

**Description** : Display several panes, vertically or horizontally in a TAChart

## TAChart_financial
**Category** : TAChart

**Keywords** : TAChart Financial 

[TAChart/TAChart_financial](TAChart/TAChart_financial)

**Description** : Displays, in a TAChart, financial data, specificially hi and low extent of share prices.

## TAChart_multi
**Category** : TAChart

**Keywords** : TAChart Multi 

[TAChart/TAChart_multi](TAChart/TAChart_multi)

**Description** : Displays some Multi charts

## TaChart_db-pieseries
**Category** : TAChart

**Keywords** : TAChart Pie Chart dbgrid 

[TAChart/TaChart_db-pieseries](TAChart/TaChart_db-pieseries)

**Description** : Draws a Pie Chart using data from a dbgrid.

## TAChart_plotunit
**Category** : TAChart

**Keywords** : TAChart plotunit runtime 

[TAChart/TAChart_plotunit](TAChart/TAChart_plotunit)

**Description** : The demo "plotunit" show how to generate a series and insert it into a chart at runtime. The main series types of TAChart are supported.

See the associated readme.txt

## fpvectorial
**Category** : TAChart

**Keywords** : TaChart TvVectorialFormat TFPVectorialDrawer 

[TAChart/fpvectorial](TAChart/fpvectorial)

**Description** : Uses Vectorial to display (and export) some line and block charts.

Note, generates a message that it has failed to open a particular TrueTypeFont that is definitely available but works anyway. 

## tachart_imagelist
**Category** : TAChart

**Keywords** : TAchart TAChartImagelist 

[TAChart/tachart_imagelist](TAChart/tachart_imagelist)

**Description** : Shows how to display images in a TAChart Image List in a TAChart.

## TAChart_dualaxes
**Category** : TAChart

**Keywords** : TAChart Dual Axes runtime 

[TAChart/TAChart_dualaxes](TAChart/TAChart_dualaxes)

**Description** : How to use dual axes (ie left and right) in a TAChart.

## TAChart_chartsource
**Category** : TAChart

**Keywords** : TAChart 

[TAChart/TAChart_chartsource](TAChart/TAChart_chartsource)

**Description** : Demonstrates TAChart with different sources.

## TAChart_animate
**Category** : TAChart

**Keywords** : TAChart Animated 

[TAChart/TAChart_animate](TAChart/TAChart_animate)

**Description** : Animate a TAChart in three different modes.

## TAChart_chartstyles
**Category** : TAChart

**Keywords** : TAChart Stacked Bar runtime 

[TAChart/TAChart_chartstyles](TAChart/TAChart_chartstyles)

**Description** : This project shows how to add and delete levels to a stacked bar series at runtime. For every new level, a new chart style is created, and for every deleted level, the corresponding chart style is deleted as well.

See the associated readme.txt for more information.





## TAChart_panes
**Category** : TAChart

**Keywords** : TAChart Panes 

[TAChart/TAChart_panes](TAChart/TAChart_panes)

**Description** : Demonstrates using several panes in a TAChart.

## TAChart_html
**Category** : TAChart

**Keywords** : TAChart html export svg 

[TAChart/TAChart_html](TAChart/TAChart_html)

**Description** : Draws a chart with HTML formatted text to enable sub-/superscripts or unusually colored text. Can save or export that chart in WMF (Windows only) or SVG. Can copy that chart, as a bitmap to clipboard (but see below).

Has problems on some Linux versions.
1. When click the SVG button, fails to load a FreeType font when running in the IDE. OK when standalone. This is due to a defective FreeType font contained in some Linux versions and which our FreeType libs cannot cope with.
2. When click "copy to clipboard", a crash is triggered (it should copy a bitmap image).

## TAChart_bgra
**Category** : TAChart

**Keywords** : TAChart bgra bitmap bgrabitmappack TAGUIConnectorBGRA BGRAGradients 

[TAChart/TAChart_bgra](TAChart/TAChart_bgra)

**Description** : Demonstrates using bgra to enhance the appearance of charts.
This demo requires both BGRABitmap from the OnLine Package Manager and TACharBGRA from the Lazarus supplied packages.


## TAChart_liveview
**Category** : TAChart

**Keywords** : TAChart live view add data 

[TAChart/TAChart_liveview](TAChart/TAChart_liveview)

**Description** : Demonstrates adding data points to a chart, in this case from a user click and a random number but obvious how to do so from some other process. 

## TAChart_3d
**Category** : TAChart

**Keywords** : TAChart 3d 

[TAChart/TAChart_3d](TAChart/TAChart_3d)

**Description** : Demonstrates some 3d charts using randomly generated data. Chart1 has three 'series' properties, each one has a data 'source' set to a TRandomChartSource.

## TAChart_dragdrop
**Category** : TAChart

**Keywords** : TAChart Drag and Drop DragDrop 

[TAChart/TAChart_dragdrop](TAChart/TAChart_dragdrop)

**Description** : Demonstrates dragging chart data points for both line and bar chart.

## TAChart_tools
**Category** : TAChart

**Keywords** : TAChart Panning Zooming 

[TAChart/TAChart_tools](TAChart/TAChart_tools)

**Description** : Demonstrates some TAChart tools, zooming and panning in various modes.

## TAChart_rotate
**Category** : TAChart

**Keywords** : TAChart rotate 

[TAChart/TAChart_rotate](TAChart/TAChart_rotate)

**Description** : How to rotate some parts of a combined bar and line chart.

## TAChart_sorted_source
**Category** : TAChart

**Keywords** : TAChart sorting 

[TAChart/TAChart_sorted_source](TAChart/TAChart_sorted_source)

**Description** : Demonstrates sorting the data that is the source for a number of different chart plots.

## TAChart_script
**Category** : TAChart

**Keywords** : TAChart Pascal Script PSCompiler 

[TAChart/TAChart_script](TAChart/TAChart_script)

**Description** : Demonstrates plotting the output of a short Pascal Script. 

## TAChart_axis
**Category** : TAChart

**Keywords** : TAChart Axis 

[TAChart/TAChart_axis](TAChart/TAChart_axis)

**Description** : Demonstrates a range of styles and types of axis on a chart.

## TAChart_save
**Category** : TAChart

**Keywords** : TAChart save file PNG SVG BMP JPG JPEG 

[TAChart/TAChart_save](TAChart/TAChart_save)

**Description** : Shows how to save a chart as a BMP, SVG, JPEG or PNG.

## barseriesshapes
**Category** : TAChart

**Keywords** : TAChart bar chart shapes 

[TAChart/barseriesshapes](TAChart/barseriesshapes)

**Description** : Demonstrates the different shapes that can be used to draw the bars in a bar chart.

## TAChart_distance
**Category** : TAChart

**Keywords** : TAChart measure distance 

[TAChart/TAChart_distance](TAChart/TAChart_distance)

**Description** : How to measure distances on a chart using the mouse.

## TAChart_datapointtools
**Category** : TAChart

**Keywords** : TAChart data point tools cross hair hints 

[TAChart/TAChart_datapointtools](TAChart/TAChart_datapointtools)

**Description** : Demonstrates a number of tools that allow you to examine and alter data points for a range of plot types.

## TAChart_userdrawn
**Category** : TAChart

**Keywords** : TAChart userdrawn 

[TAChart/TAChart_userdrawn](TAChart/TAChart_userdrawn)

**Description** : Demonstrates defining your own series draw method. The chart has a 'series' of type TUserDrawnSeries whose OnDraw event is set to call the  procedure  TMainForm.SeriesDraw. 

## TAChart_nan
**Category** : TAChart

**Keywords** : TAChart nan not a number 

[TAChart/TAChart_nan](TAChart/TAChart_nan)

**Description** : Demonstrates how TAChart deals with a data series that contain a nan (not a number). 

## TAChart_func
**Category** : TAChart

**Keywords** : TAChart function 

[TAChart/TAChart_func](TAChart/TAChart_func)

**Description** : Plot a number of functions in TAChart.

## TAChart_editor
**Category** : TAChart

**Keywords** : TAChart Chart Editor Interactive Attributes 

[TAChart/TAChart_editor](TAChart/TAChart_editor)

**Description** : Has a chart with several data series displayed, click on some aspect of it and see an interactive dialog allowing you to alter the relevant attributes. 

## TAChart_db
**Category** : TAChart

**Keywords** : TAChart dbase MemDataset 

[TAChart/TAChart_db](TAChart/TAChart_db)

**Description** : Demonstrates plotting a TAChart from data from a database.

## TaChart_combobox
**Category** : TAChart

**Keywords** : TAChart TChartCombobox 

[TAChart/TaChart_combobox](TAChart/TaChart_combobox)

**Description** : Demonstrates use of TChartCombobox, preconfigured with content useful in drawing a chart.  

## TAChart_extent
**Category** : TAChart

**Keywords** : TAChart extent zoom 

[TAChart/TAChart_extent](TAChart/TAChart_extent)

**Description** : Shows how to vary the extent of a chart that is in view, effectivly allowing you to zoom into specific parts.

## TAChart_aggpas
**Category** : TAChart

**Keywords** : TAChart aggpas Needs Work 

[TAChart/TAChart_aggpas](TAChart/TAChart_aggpas)

**Description** : Requires the package tachartaggpas to be installed before loading.

Note: Text rendering issues in cocoa. In Linux, make sure that FONT_DIR indicates the directory with your ttf fonts.

## TAChart_fit
**Category** : TAChart

**Keywords** : TAChart fit confidence function 

[TAChart/TAChart_fit](TAChart/TAChart_fit)

**Description** : Fit data to a function, calculating and plotting confidence levels. 

## TAChart_labels
**Category** : TAChart

**Keywords** : TAChart Labels 

[TAChart/TAChart_labels](TAChart/TAChart_labels)

**Description** : Demonstrates how to add labels to the chart. Places them across the top of the bars in a bar chart and in the axes of a line chart.

## TAChart_opengl
**Category** : TAChart

**Keywords** : TAChart OpenGL 

[TAChart/TAChart_opengl](TAChart/TAChart_opengl)

**Description** : Demonstrates using OpenGL in a TAChart.
Requires the package LazOpenGLContext





## TAChart_axistransf
**Category** : TAChart

**Keywords** : TAChart Axis Transform 

[TAChart/TAChart_axistransf](TAChart/TAChart_axistransf)

**Description** : Demonstrates the transformation of  a charts axis, adjusted for non linear data. 

## TAChart_wmf
**Category** : TAChart

**Keywords** : TAChart wmf Windows Only 

[TAChart/TAChart_wmf](TAChart/TAChart_wmf)

**Description** : Saves a chart to a file in WMF format.
Runs only on Windows. An alternative way to create WMF files is by using the fpvectorial drawer.

## TAChart_print
**Category** : TAChart

**Keywords** : TAChart printing 

[TAChart/TAChart_print](TAChart/TAChart_print)

**Description** : Demonstrates printing a TAChart, including via the canvas, scaled and direct.

## TAChart_legend
**Category** : TAChart

**Keywords** : TAChart Legend 

[TAChart/TAChart_legend](TAChart/TAChart_legend)

**Description** : Demonstrates various legend styles in a TAChart. Alignment, text attributes etc.

## TAChart_listbox
**Category** : TAChart

**Keywords** : TAChart listbox TAChartListBox 

[TAChart/TAChart_listbox](TAChart/TAChart_listbox)

**Description** : Demonstrates TAChartListBox, a ListBox with chart specific attributes.

## TAChart_line
**Category** : TAChart

**Keywords** : TAChart line chart pointers 

[TAChart/TAChart_line](TAChart/TAChart_line)

**Description** : Demonstrates a line chart and the various pointers it might use.

## TAChart_events
**Category** : TAChart

**Keywords** : TAChart events OnBeforeDrawBackground OnBeforeDrawBackWall BeforeDrawBar 

[TAChart/TAChart_events](TAChart/TAChart_events)

**Description** : Demonstrates how to respond to some TAChart drawing events.

## TAChart_manhattan
**Category** : TAChart

**Keywords** : TAChart manhatten genomic 

[TAChart/TAChart_manhattan](TAChart/TAChart_manhattan)

**Description** : Displays a Manhatten Plot (often used in genomics), useful for a large number of datapoints of varying amplitude.

## TAChart_panes-3
**Category** : TAChart

**Keywords** : TAChart panes-3 multiple datasets 

[TAChart/TAChart_panes-3](TAChart/TAChart_panes-3)

**Description** : Demonstrates multiple datasets in the one chart, each with its own Y axis. 

## TAChart_navigate
**Category** : TAChart

**Keywords** : TAChart navigate zoom 

[TAChart/TAChart_navigate](TAChart/TAChart_navigate)

**Description** : Demonstrates how to navigate and zoom around a chart.



## TAChart_errorbars
**Category** : TAChart

**Keywords** : TAChart ErrorBars 

[TAChart/TAChart_errorbars](TAChart/TAChart_errorbars)

**Description** : Demonstrates error bars in a chart.

## TAChart_nogui
**Category** : TAChart

**Keywords** : TAChart nogui console mode 

[TAChart/TAChart_nogui](TAChart/TAChart_nogui)

**Description** : This demo does not have a GUI, when run, its writes its chart out to a png file in the working dir.

Note: Window users must see the readme.txt about putting freetype.dll in current directory.



