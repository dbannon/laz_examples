unit uConst;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

const
  cRemoteRepository = 'https://gitlab.com/api/v4/projects/32866275/repository/';
                                         // Immediate Local dir name under which we copy or
  cExamplesDir = 'examples_work_dir';    // download examples to. Carefull about simplifying it
  cConfigFileName = 'exampleprojectscfg.xml';

resourcestring

    // --------- Multiple units
    rsExampleProjects = 'Example Projects';

    // ---------- uLaz_Examples
    rsExSearchPrompt = 'Search Here';
    rsExNoProjectFile = 'Maybe no project file ?';
    rsFoundExampleProjects = 'Found %d Example Projects';
    rsRefreshExistingExample =  'Refresh Existing Example ?';
    rsExDownloadingProject = 'Downloading Project...';
    rsExCopyingProject = 'Copying Project...';
    rsExProjectDownloadedTo = 'Project Downloaded to';      // followed by a full path name
    rsExProjectCopiedTo = 'Project Copied to';              // followed by a full path name
    rsExampleName = 'Name';                                 // Column title
    rsExamplePath = 'Path';                                 //  "
    rsExampleKeyWords = 'Key Words';                        //  "
    rsExSearchingForExamples = 'Searching for Examples';
    rsFailedToCopyFilesTo = 'Failed to copy files to';        // Followed by a dir were we, apparently, cannot write

    // These are ObjectInspector set but I believe I cannot get OI literals i18n in a Package ??
    rsExampleOpen = 'Open';                                 // Button Caption
    rsExampleDownload = 'Download';                         //  "
    rsExampleClose = 'Close';                               //  "
    rsExampleCategory = 'Category';                         //  "

    // Settings Frame
    rsDirWhereExamplesGo = 'Directory where Examples go';

    // ------- rsExampleData
    // Most literals in uExampleData are for debugging only and very unlikely to be
    // seen by the end user. But a couple of network related ones may need i18n -
    rsExNetWorkError = 'GitLab NetWork Error';              // Followed by system error msg


implementation

end.

