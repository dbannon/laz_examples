unit uIntf;

{$mode objfpc}{$H+}

interface

uses
  Classes,
  //LCL,
  LCLType,
  //IDEIntf,
  MenuIntf, IDECommands, ToolBarIntf, IDEOptEditorIntf;

procedure Register;

implementation

uses uLaz_Examples, uConst, lazlogger,
  LazIDEintf, LazFileUtils, BuildIntf, ExWinSettings,
  baseIDEIntf, IDEOptionsIntf, LazConfigStorage, SysUtils;


function GetExamplesDirectory() : string;                    // NO, this is wrong, it should be config !!!!
var
  Config: TConfigStorage;
begin
    try
      Config := GetIDEConfigStorage(cConfigFileName, true);
      try
        Result := Config.GetValue('Examples/Directory',
                                IDEEnvironmentOptions.GetParsedLazarusDirectory);
      finally
        Config.Free;
      end;
    except
      on E: Exception do begin
        DebugLn('Examples UIntf GetExamplesDirectory Loading ' +  cConfigFileName + ' failed: ' + E.Message);
        Result := IDEEnvironmentOptions.GetParsedLazarusDirectory;
      end;
    end;

end;

procedure IDEMenuSectionClicked(Sender: TObject);
var
  ProjectFFile : string;
begin
  FormLazExam := TFormLazExam.Create(nil);
  try
    FormLazExam.ExamplesHome := GetExamplesDirectory();
    FormLazExam.RemoteRepo := cRemoteRepository;
    FormLazExam.ShowModal;
    ProjectFFile := FormLazExam.ProjectToOpen;
  finally
    FormLazExam.Free;
    FormLazExam := nil;
  end;
  if ProjectFFile <> '' then
    LazarusIDE.DoOpenProjectFile(ProjectFFile, [ofProjectLoading]);
end;

procedure Register;
var
  IDEShortCutX: TIDEShortCut;
  IDECommandCategory: TIDECommandCategory;
  IDECommand: TIDECommand;
begin
  IDEShortCutX := IDEShortCut(VK_E, [ssCtrl, ssAlt], VK_UNKNOWN, []);
  IDECommandCategory := IDECommandList.FindCategoryByName('ToolMenu');
  IDECommand := nil;
  if IDECommandCategory <> nil then
  begin
    IDECommand := RegisterIDECommand(IDECommandCategory, rsExampleProjects, rsExampleProjects, IDEShortCutX, nil, @IDEMenuSectionClicked);
    if IDECommand <> nil then
      RegisterIDEButtonCommand(IDECommand);
  end;
  RegisterIDEMenuCommand(itmSecondaryTools, rsExampleProjects, rsExampleProjects + ' ...', nil, @IDEMenuSectionClicked, IDECommand, 'pkg_oep');
  RegisterIDEMenuCommand(ComponentPalettePageDropDownExtraEntries, rsExampleProjects, rsExampleProjects + ' ...', nil, @IDEMenuSectionClicked, nil, 'pkg_oep');

  ExWinOptionsFrameID := RegisterIDEOptionsEditor(ExWindowOptionsGroup, TExWinSettingsFrame, 9999)^.Index;  // AIndex = what ???

end;

initialization


finalization


end.

