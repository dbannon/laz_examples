unit uChangeStuff;

{$mode ObjFPC}{$H+}

interface

uses
    Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

    { TFormChangeStuff }

    TFormChangeStuff = class(TForm)
        ButtonStart: TButton;
        CheckDryRun: TCheckBox;
        Edit1: TEdit;
        Edit2: TEdit;
        GroupBox1: TGroupBox;
        GroupBox2: TGroupBox;
        Label1: TLabel;
        Label2: TLabel;
        LabelStartingDir: TLabel;
        Memo1: TMemo;
        RadioCategoryIs: TRadioButton;
        RadioAKeyWordIs: TRadioButton;
        procedure ButtonStartClick(Sender: TObject);
    private
        procedure ProcessJsonCat(FFname: string);
        procedure ProcessJsonKey(FFname: string);

    public


    end;

var
    FormChangeStuff: TFormChangeStuff;
    Cnt : integer = 0;

implementation

uses FileUtil, fpJSON, jsonparser, jsonscanner, lazlogger;

{$R *.lfm}

{ TFormChangeStuff }

procedure TFormChangeStuff.ProcessJsonCat(FFname : string) ;
var
    J : TJSONData;
    F : TFileStream;
    JObject : TJSONObject;
    jStr : TJSONString;
    STL : tstringList;
begin
    F:=TFileStream.Create(FFname,fmOpenRead);
    try
        with TJSONParser.Create(F, [joUTF8, joIgnoreTrailingComma]) do
            try
                J:=Parse;
            Finally
                Free;
            end;
    finally
        F.FRee;
    end;
    JObject := TJSONObject(J.Items[0]);      // this JSON has one stanza, its 0
    if jObject.Find('Category', Jstr) then begin
        if jStr.AsString = Edit1.text then begin
            JObject.Elements['Category'].AsString := Edit2.text;
            STL := TStringList.Create;
            STL.Text := J.FormatJSON();
            if not CheckDryRun.Checked then
                //STL.SaveToFile('/tmp/blar.ex-meta');
                STL.SaveToFile(FFname);
            STl.Free;
            inc(Cnt);
        end;
    end else debugln('ERROR - no Category for ' + FFName);
    J.free;
end;

procedure TFormChangeStuff.ProcessJsonKey(FFname : string) ;
var
    J : TJSONData;
    F : TFileStream;
    JObject : TJSONObject;
    jStr : TJSONString;
    STL : tstringList;
    JArray : TJsonArray;
    i : integer;
begin
    F:=TFileStream.Create(FFname,fmOpenRead);
    try
        with TJSONParser.Create(F, [joUTF8, joIgnoreTrailingComma]) do
            try
                J:=Parse;
            Finally
                Free;
            end;
    finally
        F.FRee;
    end;
    JObject := TJSONObject(J.Items[0]);      // this JSON has one stanza, its 0
    //debugln('Have an object');
    if jObject.Find('Keywords', JArray) then begin
        if JArray = nil then writeln('But its NIL');
        //debugln('Found some keywords - ' {+ jArray.AsString});
        i := 0;
        while pos(Edit1.Text, jArray[i].AsString) < 1 do begin
            inc(i);
            if i >= JArray.count then break;
        end;
        if i < JArray.count then begin
            //debugln('Found THE keyword');
            JObject.Elements['Category'].AsString := Edit2.text;
            STL := TStringList.Create;
            STL.Text := J.FormatJSON();
            if not CheckDryRun.Checked then
                // STL.SaveToFile('/tmp/blar.ex-meta');
                 STL.SaveToFile(FFname);
            STl.Free;
            inc(Cnt);
        end;
    end else debugln('WARNING - no KeyWords for ' + FFName);
    J.free;
end;

procedure TFormChangeStuff.ButtonStartClick(Sender: TObject);
var
    MetaFiles: TStringList;
    St : string;

begin
    Memo1.Clear;
    Cnt := 0;
    MetaFiles := TStringList.Create;
    try
        FindAllFiles(MetaFiles, LabelStartingDir.Caption, '*.ex-meta', True);
        for St in MetaFiles do begin
            //debuglnln(St);
            //Memo1.Lines := MetaFiles;
            if RadioCategoryIs.Checked then
                ProcessJsonCat(St)
            else ProcessJsonKey(St);
        end;
        memo1.append('Files processed = ' + inttostr(MetaFiles.Count) + ' and ' + inttostr(cnt) + ' actioned');
    finally
        MetaFiles.Free;
    end;
end;

end.

