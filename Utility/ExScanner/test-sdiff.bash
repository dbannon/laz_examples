#!/bin/bash
OTHER="/home/dbannon/bin/Lazarus/lazarus-drb/components/exampleswindow/"

echo "--------- LOCAL VERSION --------- ulaz_examples.lfm ---------------- GITLAB VERSION ---------"
sdiff -bs ulaz_examples.lfm "$OTHER"ulaz_examples.lfm
echo "--------- LOCAL VERSION --------- ulaz_examples.pas ---------------- GITLAB VERSION ---------"
sdiff -bs ulaz_examples.pas "$OTHER"ulaz_examples.pas
echo "--------- LOCAL VERSION --------- uexampledata.pas ---------------- GITLAB VERSION ---------"
sdiff -bs uexampledata.pas  "$OTHER"uexampledata.pas
echo "--------- LOCAL VERSION ------------- uconst.pas ---------------- GITLAB VERSION ---------"
sdiff -bs uconst.pas        "$OTHER"uconst.pas

echo "Other Dir = $OTHER"

