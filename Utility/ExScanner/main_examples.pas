unit Main_Examples;

{ This using may not have a long life, its here now to do thwo things -

1. Demonstrate the prototype Examples Window that may be added to Lazarus

2. Help with processing the existing help projects to add the necessary
   metadata and generate a master metadata file that end users can download.
   And generate a markdown summary of our projects.

   David Bannon, Jan 2022


   OK, new mode now that ~/examples is done and we are adding metadata to examples
   in place.
   1. When testing, don't build examples in the git tree, cannot leave files lying around
      so, work with Lazarus 2.2.0 where we don't care if we add some files.
   2. But we want to save the MetaFile in the uploadable Git Tree, so, we will start by
      browsing this app in the "Local Lazarus Install", if user opens a dir we
      look for a README.txt, open it if it exists, else just prepare to write
      a metafile there.

}




{$mode ObjFPC}{$H+}




interface



uses
    Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
    ComCtrls, Buttons, ListViewFilterEdit, uexampledata{, reclist };  // ????

type

    { TForm2 }

    TForm2 = class(TForm)
        ButtonDumpDesc: TButton;
        ButtonSpellCheck: TButton;

        ButtonChangeStuff: TButton;
        ButtonSave: TButton;
        ButtonSetExSource: TButton;
        ButtonEditMeta: TButton;
        ButtonInstallMeta: TButton;
        ButtonUserWindow: TButton;
        ComboJCategory: TComboBox;
        EditJName: TEdit;
        EditJKeyWords: TEdit;
        EditStandAloneGit: TEdit;   // Where we build a stand alone tree to later upload to Git for online viewing
        EditLazConfigDir: TEdit;    // Where user window will copy Examples to.
        EditLocalGitDir: TEdit;     // is the Laz Src git tree we write back up to gitlab, save metadate here
        EditRemoteRepo: TEdit;      // Is the remote, online repo, not being used.
        GroupBox1: TGroupBox;
        GroupBoxMetaFile: TGroupBox;
        Label1: TLabel;
        Label2: TLabel;
        Label3: TLabel;
        Label4: TLabel;
        Label5: TLabel;
        Label6: TLabel;
        Label7: TLabel;
        Label8: TLabel;
        Memo1: TMemo;
        OpenDialog1: TOpenDialog;
        Panel1: TPanel;
        SelectDirectoryDialog1: TSelectDirectoryDialog;
        StatusBar1: TStatusBar;
        procedure ButtonChangeStuffClick(Sender: TObject);
        procedure ButtonDumpDescClick(Sender: TObject);
        procedure ButtonSaveClick(Sender: TObject);
        procedure ButtonEditMetaClick(Sender: TObject);
        procedure ButtonInstallMetaClick(Sender: TObject);
        procedure ButtonSetExSourceClick(Sender: TObject);
        procedure ButtonSpellCheckClick(Sender: TObject);
        procedure ButtonUserWindowClick(Sender: TObject);
        procedure ComboJCategoryChange(Sender: TObject);
        procedure EditJKeyWordsChange(Sender: TObject);
        procedure EditJNameChange(Sender: TObject);
        procedure FormCreate(Sender: TObject);
        procedure FormDestroy(Sender: TObject);
        procedure FormResize(Sender: TObject);
        procedure FormShow(Sender: TObject);
        procedure Memo1Change(Sender: TObject);
    private  { REc : tRecordList ; }
        function GetLocalTimeISO8601: ANSIstring;
        function CopyAllExamples(const EX: TExampleData): integer;
        function CopyExDirectory(const SrcDir, DestDir: string): boolean;
        procedure DisplayMetaFile(DirName: string);
        procedure LoadMetaFile(FFName: string);
        function LooksLikeSRCTree(SuggestedDir: string): boolean;
        function SaveString(const St, FFName: string; out Error: string): boolean;
        procedure SetSaveStatus();
        procedure SetSomeDefaults();
        function WriteMasterMeta(const EX: TExampleData; const FFName: string
            ): boolean;
        procedure WriteMDFile(const EX: TExampleData; const FFName: string);

    public

    end;

const
    // These are MY defaults, local ones will always have $HOME prepended before use.

    { Was (hopefully recently) pulled down from from official Lazarus, we read
    examples from here and copy them to DEFAULT_LAZ_UP_GIT }
    DEFAULT_LAZ_DOWN_GIT = '/bin/Lazarus/lazarus/';

    // This git directory is pushed UP to the online unofficial gitlab Example Repo after
    // being populated by emt. It also contains emt src.
    DEFAULT_LAZ_UP_GIT = '/Pascal/laz_examples/';

    // Where we save downloaded/copied projects while testing and find OPM projects.
    DEFAULT_LAZ_CONFIG = '/bin/LazConfig/lazrus-main/';

    // The remote Gitlab repo that holds online Examples
    DEFAULT_REMOTE_EXAMPLES = 'https://gitlab.com/api/v4/projects/32866275/repository/';


var
    Form2: TForm2;

implementation

{$R *.lfm}
{$WARN 6058 off : Call to subroutine "$1" marked as inline is not inlined}

uses uLaz_Examples, LazFileUtils, FileUtil, LCLProc, uConst, uChangeStuff
    {$ifdef LINUX},uDebSpell, Unix {$endif},        // We call a ReReadLocalTime();
    uDump;
var
    Home : string;
    SRCFiles : TstringList = nil;  // may have a list of file to copy to git dir

const
  GroupMetaCaption = 'Meta File';
  GETTINGSTARTED =  'Probably click the Set Button';

{ TForm2 }

procedure TForm2.ButtonUserWindowClick(Sender: TObject);
begin
    FormLazExam.GitDir       := appendPathDelim(EditLocalGitDir.Text);
    FormLazExam.LazConfigDir := appendPathDelim(EditLazConfigDir.Text);
    FormLazExam.RemoteRepo   := appendPathDelim(EditRemoteRepo.Text);
    FormLazExam.ExamplesHome := appendPathDelim(EditLazConfigDir.Text) +cExamplesDir + PathDelim;
    StatusBar1.SimpleText
        := 'Will load up a master meta file, downloading from remote if necessary, scans for OPM projects too';
    Application.ProcessMessages;
    // FormLazExam.EditSearch.Text := 'image';                           // test Keyword search preload
    FormLazExam.ShowModal;
    if FormLazExam.ProjectToOpen <> '' then
        ShowMessage('User wants to open ' + FormLazExam.ProjectToOpen);
end;

procedure TForm2.SetSaveStatus();
begin
    ButtonSave.Enabled := (ComboJCategory.text <> '') and (Memo1.Text <> '');
end;

procedure TForm2.ComboJCategoryChange(Sender: TObject);
begin
    SetSaveStatus();
end;

procedure TForm2.EditJKeyWordsChange(Sender: TObject);
begin
    SetSaveStatus();
end;

procedure TForm2.EditJNameChange(Sender: TObject);
begin
    SetSaveStatus();
end;

procedure TForm2.Memo1Change(Sender: TObject);
begin
    SetSaveStatus();
end;

function TForm2.GetLocalTimeISO8601: ANSIstring;
var
   ThisMoment : TDateTime;
   Res : ANSIString;
   Off : longint;
begin
    {$ifdef LINUX}
    ReReadLocalTime();    // in case we are near daylight saving time changeover
    {$endif}
    ThisMoment:=Now;
    Result := FormatDateTime('YYYY-MM-DD',ThisMoment) + 'T'
                    + FormatDateTime('hh:mm:ss',ThisMoment);
    Off := GetLocalTimeOffset();
    if (Off div -60) >= 0 then Res := '+'
	else Res := '-';
	if abs(Off div -60) < 10 then Res := Res + '0';
	Res := Res + inttostr(abs(Off div -60)) + ':';
       	if (Off mod 60) = 0 then
		Res := res + '00'
	else Res := Res + inttostr(abs(Off mod 60));
    Result := Result + res;
end;

const
    LastUpDate = 'LastUpDate';              // Name of JSON item were we store last update

function TForm2.WriteMasterMeta(const EX : TExampleData; const FFName : string) : boolean;
var
   i : integer;
   FirstRun : boolean = True;
   STL : TStringList;
   St, StIndexed : string;
   Proj, Cat, Path, KeyW, Desc : string;
begin
    STL := TStringList.Create;
    StL.Add('{'#10'"' + LastUpDate + '":"' + GetLocalTimeISO8601() +'",');
    while Ex.GetListData(Proj, Cat, Path, KeyW, FirstRun) do begin
        if FirstRun then FirstRun := False;
        StL.Add('"' + EX.EscJSON(Cat + '/' + Proj) + '" : {');
        StL.Add('  "Name" : "' + Ex.EscJSON(Proj) + '",');                         // Must be unique
        StL.Add('  "Category" : "' + Ex.EscJSON(Cat) + '",');
        // ---------------------------------------------------------------------
        // Ugly Hack - I have to turn the keywords back into a JSON Array
        //----------------------------------------------------------------------
        {St := '';
        for StIndexed in ExList.Items[i]^.Keywords do
            St := St + '"' + StIndexed + '",';
        if St.Length > 0 then delete(St, St.Length, 1);                         // Remove trailing comma
        StL.Add('  "Keywords" : [' + St + '],');  }
        //StL.Add('  "Keywords" : [' + KeyW + '],');
        StL.Add('  "Keywords" : [' + 'Keywords Not Valid see TForm2.WriteMasterMeta' + '],');
        Desc := Ex.GetDesc(Path + Proj);                          // seems to be relative ...
        if Desc = '' then begin
            Showmessage('Error writing master file, see console output');
            debugln('TForm2.WriteMasterMeta ======================');
            debugln(STL.text);
            STL.free;
            exit(False);
        end;
        StL.Add('  "Description" : "' + Ex.EscJSON(Desc) + '"},');
    end;
    if STL.Count > 1 then begin
        St := STL[STL.Count-1];
        delete(St, St.Length, 1);
        STL[STL.Count-1] := St;
    end;
    Stl.Add('}');
    deletefile(FFName);        // ToDo : test its there first and then test delete worked
    STL.SaveToFile(FFName);
    STL.Free;
    Result := fileexists(FFName);
end;


procedure TForm2.WriteMDFile(const EX : TExampleData; const FFName : string);
var
    MD : TStringList;
    FirstRun : boolean = True;
    Proj, Cat, Path, KeyW : string;
begin
    MD := TStringList.Create;
    while Ex.GetListData(Proj, Cat, Path, KeyW, FirstRun) do begin
        if FirstRun then FirstRun := False;
        MD.Add('## ' + Proj);                        // they are so initialized !
        MD.Add('**Category** : ' + Cat + #10);
        MD.Add('**Keywords** : ' + KeyW + #10);
        //MD.Add('[' + Path + '](' + Path + ')' + #10);
        MD.Add('[' + Cat + '/' + Proj + '](' + Cat + '/' + Proj + ')' + #10);
        MD.Add('**Description** : ' + Ex.GetDesc(Path + Proj) + #10);
    end;
    MD.SaveToFile(FFName);
    MD.Free;
end;

// Copies all the files, including subdirectories to indicated dir
function TForm2.CopyExDirectory(const SrcDir, DestDir : string) : boolean;
var
    STL : TStringList;
    St  : string;
    ChopOff : integer;
begin
    ChopOff := length(AppendPathDelim(SrcDir));
    if not ForceDirectoriesUTF8(DestDir) then exit(False);
    STL := FindAllDirectories(SrcDir, True);
    try
        for St in STL do
            // note the copy process leaves a leading Pathdelim, good, I think...
            if not ForceDirectoriesUTF8(DestDir + copy(St, ChopOff, 1000)) then exit(False);
            debugln('CopyExDirectory Forced Dir ' + St);
    finally
        STL.Free;
    end;
    STL := FindAllFiles(SrcDir, AllFilesMask, True, faAnyFile);
    try
        for St in STL do begin
            if not copyfile(St, DestDir + copy(St, ChopOff, 1000)) then exit(False);
            //debugln('TFormLazExam.CopyFiles Copy ' + ST + #10 + ' to ' + DestDir + Proj + copy(St, ChopOff, 1000));  // DRB
            debugln('CopyExDirectory Copied Dir ' + St);
        end;
    finally
        STL.Free;
    end;
end;

function TForm2.CopyAllExamples(const EX : TExampleData) : integer;
var
    FirstRun : boolean = True;
    Proj, Cat, Path, KeyW : string;
    Categories : TStringList;
begin
    Result := 0;
    debugln('TForm2.CopyAllExamples');
    Categories := TStringList.Create;
    ex.getCategoryData(Categories);
    ex.CatFilter := Categories.Text;
    while Ex.GetListData(Proj, Cat, Path, KeyW, FirstRun) do begin
        if FirstRun then FirstRun := False;
        debugln('CopyAllExamples copying ' + Proj);
        if not CopyExDirectory(EditLocalGitDir.Text + Path, EditStandAloneGit.text
                                + Cat + PathDelim + Proj + PathDelim) then begin
            Showmessage('Failed copy - ' + Path + ' to ' + Cat);
            debugln('SOURCE ' + EditLocalGitDir.Text + Path);
            debugln('DESTIN ' + EditStandAloneGit.text + Cat + PathDelim + Proj + PathDelim);
            exit;
        end;
        inc(result);
    end;
    Categories.free;
end;

        // https://gitlab.com/dbannon/laz_examples/-/tree/main/Beginner
        // https://gitlab.com/dbannon/laz_examples/-/tree/main/Beginner/customhint/

procedure TForm2.ButtonInstallMetaClick(Sender: TObject);
var
    Ex : TExampleData;
begin
    Screen.Cursor := crHourGlass;
    EditLocalGitDir.Text := appendPathDelim(EditLocalGitDir.Text);      // just to be sure
    EditLazConfigDir.Text := appendPathDelim(EditLazConfigDir.Text);
    EditStandAloneGit.Text := appendpathdelim(EditStandAloneGit.Text);
    if not DirectoryExists(EditStandAloneGit.Text) then
        ForceDirectory(EditStandAloneGit.Text);                  // we will need that.
    Ex := TExampleData.Create();
    try
        Ex.GitDir := EditLocalGitDir.Text;
        Ex.LazConfigDir := EditLazConfigDir.Text;                // ??
        Ex.RemoteRepo := appendPathDelim(EditRemoteRepo.Text);   // ??
        Ex.LoadExData(FromLocalTree);                            // Uses  Ex.GitDir := setting above, eg Laz Src Tree
        if Ex.ErrorMsg <> '' then
            ShowMessage(Ex.ErrorMsg)
        else StatusBar1.SimpleText := 'Found ' + inttostr(Ex.Count) + ' examples, master file in git dir.';
        CopyAllExamples(Ex);                                     // Note : also sets CatFilter, should move to here.
        WriteMDFile(EX, EditStandAloneGit.text + 'projects.md');
        WriteMasterMeta(Ex, EditStandAloneGit.text + 'master' + '.ex-meta');
        //Ex.WriteMasterMeta(EditStandAloneGit.text + 'master' + '.ex-meta');
    finally
        Ex.Free;
        Screen.Cursor := crDefault;
    end;
end;

// ---------------------  Manage A Meta File Methods ---------------------------

{ Multifunction method -
    In each case, first we test JSON.
  * If Set (ie GroupBoxMetaFile.Caption has dir) copy previously read dir
    contents to Git Dir under its category.
  * If we loaded file as an edit, we save it back to same place. So, Caption
    for this button will read the 'Save'. GroupBoxMetaFile.Caption will be file to be saved.
  * If we got here from New, without a set (GroupBoxMetaFile.Caption = GroupMetaCaption
    then we will need to ask user where they want it saved. So, caption
    for this button will change from Copy to Save.
  So, at start, user will see Set, New and Edit. Copy will be disabled.
}

function TForm2.SaveString(const St, FFName : string; out Error : string) : boolean;
var
    MemBuffer      : TMemoryStream;
begin
    //debugln('TForm2.SaveString -------------' + FFname);
    //debugln(St);

    //showmessage('Would save '+ FFName);
    //exit(true);

    if fileexistsUTF8(FFName) then
        deleteFile(FFName);
    if fileexists(FFName) then begin
        Error := 'Cannot replace ' + FFName;
        exit(False);
    end;
    MemBuffer := TMemoryStream.Create;                   // ToDo : Must check for errors here
    MemBuffer.Write(St[1], St.length);
    MemBuffer.SaveToFile(FFName);
    MemBuffer.Free;
    result := fileexists(FFName);
end;


procedure TForm2.ButtonSaveClick(Sender: TObject);      // This is also the 'save' button
var
    Ex : TExampleData;
    ErrorLine : string = '';
    FName, DName, St, Category, JContent : string;
    CurrentSRCTree : string;


(*    procedure ClickOnCopy;              // Sub procedures broken out to make it easier to read.
    begin
           // The caption is a path to a dir, no file, so last element is last dir name.
        //debugln('TForm2.ButtonCopyClick - ClickOnCopy');
        FName := extractfilename(GroupBoxMetaFile.Caption);
        DName := appendPathDelim(EditLocalGitDir.Text) + Category + PathDelim + FName;

        if directoryexists(DName) then
            showmessage('already exits, on your head it be')
        else ForceDirectory(DName);
        for St in SRCFiles do begin                     // ToDo : need to recurse down subdirectories
            if not copyfile(St, appendpathdelim(DName) + extractfilename(St)) then
            {DebugLn('copied ' + St + ' to ' + appendpathdelim(DName) + extractfilename(St))
            else }
                debugln('FAILED copy ' + St + ' to ' + appendpathdelim(DName) + extractfilename(St));
        end;
        if SaveString(JContent, appendpathdelim(DName) + FName + MetaFileExt, ErrorLine) then begin
            Memo1.clear;
            EditJname.text := GETTINGSTARTED;
            ComboJCategory.Text := '';
            EditJKeywords.text := '';

            ButtonSave.Enabled := False;
            GroupBoxMetaFile.Caption := GroupMetaCaption;
            StatusBar1.SimpleText := 'Copied ' + inttostr(SRCFiles.count) + ' Files plus meta file';
            FreeAndNil(SRCFiles);
        end else showmessage('CopyOnClick - ' + ErrorLine);
    end;                                                                     *)

    procedure ClickOnCopyEdit();     // its actually, now, end of Save workflow
    var
        FPathName : string;
    begin
        // If the original ExampleProj dir was (eg) 'example' then it will have to be
        // changed to suit the new project name. GroupBoxMetaFile.Caption contains the
        // full path to old project, EditJName.Text has new project name.
        // ToDo : what if user edits the project name and there is an existing, loaded, metafile ?

        FPathName := appendPathDelim(GroupBoxMetaFile.Caption);
(*
        if lowercase(EditJName.Text) <> lowercase(extractFileName(GroupBoxMetaFile.Caption)) then begin
            showmessage('Project rename required');
            FPathName := copy(GroupBoxMetaFile.Caption, 1, length(GroupBoxMetaFile.Caption)
                            - length(ExtractFileName(GroupBoxMetaFile.Caption)));
            FPathName := AppendPathDelim(FPathName + EditJName.Text);
            if not renamefile(appendPathDelim( GroupBoxMetaFile.Caption), FPathName) then begin
                Showmessage('Proj Dir Rename Failed');
                debugln('ClickOnCopyEdit From ' + GroupBoxMetaFile.Caption);
                debugln('Error Not changed to ' + AppendPathDelim(FPathName + EditJName.Text));
                StatusBar1.SimpleText := 'Failed to rename project dir';
                exit();
            end;
        end;       *)
        if SaveString(JContent, FPathName + EditJName.Text + MetaFileExt, ErrorLine) then begin
            StatusBar1.SimpleText := 'Updated ' + FPathName + EditJName.Text + MetaFileExt;
            DisplayMetaFile(FPathName);
            ButtonSave.Enabled := false;
        end
        else Showmessage(ErrorLine);
    end;

(*    procedure ClickOnCopyNoSet;
    begin
        debugln('TForm2.ButtonCopyClick - ClickOnCopyNoSet');
        SelectDirectoryDialog1.InitialDir := Home;
        if SelectDirectoryDialog1.execute then begin
            FName := SelectDirectoryDialog1.FileName + PathDelim
                    + ExtractFileNameOnly(SelectDirectoryDialog1.FileName)
                    + MetaFileExt;           // Defined in ExampleData Unit.
            StatusBar1.SimpleText := 'Saving to ' + FName;
            try
                // Memo1.Lines.SaveToFile(FName);
                SaveString(JContent,FName, ErrorLine);
            except on E: EStreamError do
                showmessage('Cannot Save ' + FName + ' : ' + E.Message);
            end;
        end;
    end;     *)

begin
    JContent := '{ "' + TExampleData.EscJSON(EditJName.Text) + '" : {'
             +     #10'   "Category" : "' + TExampleData.EscJSON(ComboJCategory.text)
             + '",'#10'   "Keywords" : [' + EditJKeyWords.Text + ']'
             +  ','#10'   "Description" : "' + TExampleData.EscJSON(Memo1.Text)
             + '"}'#10'}';                                    // Now contains a JSON File
    Ex := TExampleData.Create();
    CurrentSRCTree := copy(GroupBoxMetaFile.Caption, 1, length(GroupBoxMetaFile.Caption) - length(ExtractFileName(GroupBoxMetaFile.Caption)) -1);
    while CurrentSRCTree <> '' do begin
        if LooksLikeSRCTree(CurrentSRCTree) then break;
        CurrentSRCTree := copy(CurrentSRCTree, 1, length(CurrentSRCTree) - length(ExtractFileName(CurrentSRCTree)) -1);
    end;
//    if CurrentSRCTree <> '' then debugln('Found LazTree ' + CurrentSRCTree)
//    else debugln('No LazTree Found ' + CurrentSRCTree);

    try
        Ex.GitDir := CurrentSRCTree;
        Ex.LazConfigDir := EditLazConfigDir.Text;
        Ex.LoadExData(FromLocalTree);
        if Ex.ErrorMsg <> '' then begin
            showmessage(Ex.ErrorMsg);
            exit;
        end;

        debugln('TForm2.ButtonSaveClick - Found ' + inttostr(Ex.Count) + ' existing entries');
        if not EX.TestJSON(JContent, ErrorLine, Category) then begin    // TestJSON is not a class method
           showmessage('TForm2.ButtonCopyClick - ' + ErrorLine);
           debugln('TForm2.ButtonCopyClick found some bad JSON');
           debugln(JContent);
           exit;
        end;
    finally
        Ex.Free;
    end;
    ClickOnCopyEdit();
end;

procedure TForm2.ButtonChangeStuffClick(Sender: TObject);
begin
     FormChangeStuff.LabelStartingDir.caption := EditLocalGitDir.text;
     FormChangeStuff.ShowModal;
end;

procedure TForm2.ButtonDumpDescClick(Sender: TObject);
begin
    FormDump.Showmodal;
end;

// Rets T if passed string looks like its the top of Lazarus Tree
function TForm2.LooksLikeSRCTree(SuggestedDir : string) : boolean;
var
     Info : TSearchRec;
begin
    Result := True;
    if FindFirst(appendPathDelim(SuggestedDir) + 'localize.bat', faAnyFile,Info) <> 0 then
        exit(False)
    else FindClose(Info);
    if FindFirst(appendPathDelim(SuggestedDir) + 'doceditor', faAnyFile,Info) <> 0 then
        exit(False)
    else FindClose(Info);
    if FindFirst(appendPathDelim(SuggestedDir) + 'designer', faAnyFile,Info) <> 0 then
        exit(False)
    else FindClose(Info);
    if FindFirst(appendPathDelim(SuggestedDir) + 'packager', faAnyFile,Info) <> 0 then
        exit(False)
    else FindClose(Info);
end;


procedure TForm2.LoadMetaFile(FFName : string);
var
    STL : TStringList;
    Error, Desc, Keys, Cat, ExName : string;
begin
    STL := TStringList.Create;
    try
        STL.LoadFromFile(FFName);
        if not Ex.ExtractFieldsFromJSON(STL.Text, ExName, Cat
                                , Keys , Desc, Error) then
            showmessage(Error); // We will proceed, some may be usable.
        Memo1.Text := Desc;
        FormDump.InString := Desc;      // Thats in case user presses the dump button later on
        EditJKeywords.Text := Keys;
        ComboJCategory.Text := Cat;
        EditjName.Text := ExName;
    finally
        STL.Free;
    end;
end;

procedure TForm2.ButtonEditMetaClick(Sender: TObject);
//var
//    EX : TExampleData;
begin
//    EX := TExampleData.Create;                     // ToDo : why ?
    OpenDialog1.Filter := '*' + MetaFileExt;
    try
        if OpenDialog1.Execute then
            LoadMetaFile(OpenDialog1.FileName);
    finally
//        Ex.Free;
    end;
    GroupBoxMetaFile.Caption := OpenDialog1.FileName;
    ButtonSave.Caption := 'Save';
    ButtonSave.Enabled := True;
end;

// Load a metafile (that must exist) in nominated dir, display. Slow but harmless.
// Incidently, check for duplicate project names and ensure dir has only one meta file.
procedure TForm2.DisplayMetaFile(DirName : string);
var
    i, NumbLPI : integer;
    St : string;
begin
    if SRCFiles <> nil then
        SRCFiles.free;
    SRCFiles := FindAllFiles(DirName, '*', False);
    GroupBoxMetaFile.Caption := DirName;
    EditJName.Text := ExtractFileName(GroupBoxMetaFile.Caption);   // will get last dir name
    if  (EditJName.Text = 'example') or (EditJName.Text = 'demo') or (EditJName.Text = 'sample') then begin  // we need more complexity
        // sometimes they have a distinct project name (ie not project1), can we use that ?
        // else we try the next directory up.
        St := copy(GroupBoxMetaFile.Caption, 1, length(GroupBoxMetaFile.Caption) - length(EditJName.Text) -1);
        EditJName.Text := ExtractFileName(St) + '_' + EditJName.Text;
        // if this happened, we will rename the project dir when adding metadata (ie Save)  - Hmm, not any more ....
    end;
    NumbLPI := 0;
    for i := 0 to SRCFiles.Count-1 do
        if lowercase(ExtractFileExt(SRCFiles[i])) = '.lpi' then
            inc(NumbLPI);
    if NumbLPI = 0 then begin
        showmessage('No, that dir does not contain a project');
        exit;
    end;
    if NumbLPI > 1 then begin
        showmessage('No, that dir contains more than one project');
        exit;
    end;
    for i := 0 to SRCFiles.Count-1 do
        if pos({EditJName.Text + }MetaFileExt, SRCFiles[i]) > 0  then begin     // Any metafile will do now.
            LoadMetaFile(SRCFiles[i]);
            SRCFiles.Delete(i);               // why ?
            break;
        end;                        // Its not an error if we don't find it.
    if Memo1.Text = '' then        // look for a readme if no metafile there already.
        for i := 0 to SRCFiles.Count-1 do begin
            St := ExtractFileName(SRCFiles[i]);
            if St.LowerCase(St) = 'readme.txt' then begin
                Memo1.Lines.LoadFromFile(SRCFiles[i]);
                break;
            end;
        end;

end;



procedure TForm2.ButtonSetExSourceClick(Sender: TObject);

begin
    // An error needs be reported if -
    // 1. The dir contains more than one .lpi file
    // 2. The dir contains more than one metadata file.

    SelectDirectoryDialog1.InitialDir := EditLocalGitDir.Text;
    SelectDirectoryDialog1.Title := 'Select an Example Directory';
    if SelectDirectoryDialog1.execute then begin
        ButtonSave.Enabled := False;
        ButtonSave.Caption := 'Save';
        Memo1.Clear;
        EditJKeyWords.Text := '';
        ComboJCategory.ItemIndex := -1;
        DisplayMetaFile(SelectDirectoryDialog1.filename);
        StatusBar1.SimpleText := 'OK, now test example and come back here ...';  // that no longer makes sense ...
    end;
end;

// This is pretty ordinary to be honest, the report on bad spellings goes to console
// because I use linux and thats easy. So, if using in Windows, capture the output.
procedure TForm2.ButtonSpellCheckClick(Sender: TObject);
var
    DebSpell : TDebSpell;
    Ex : TExampleData;
    Correction : string;
    Index : integer = 0;
    Cnt : integer = 0;
    Proj, Cat, Path, keys : string;
    Categories : TStringList;

    function CheckTheSpelling(St, FPath : string) : boolean;
    begin
        result := true;
        Index := DebSpell.CheckThis(lowercase(St), Correction);
        if Index > 0 then begin
            debugln('');
            debugln('--------- SPELLING ' + inttostr(Index) + ' ' + FPath);
            debugln(Correction + ' >>> ' + St);
            inc(Cnt);
        end;
    end;

begin
    {$ifndef LINUX}          // Windows/Mac probably will not have Debian's spelling list
    showmessage('Sorry, linux only feature');
    exit;
    {$endif}
    DebSpell := TDebSpell.create;

    Ex := TExampleData.Create();
    try
        Ex.GitDir := EditLocalGitDir.Text;
        Ex.LazConfigDir := EditLazConfigDir.Text;
        Ex.LoadExData(FromLocalTree);
        Categories := TStringList.Create;
        ex.getCategoryData(Categories);
        ex.CatFilter := Categories.Text;         // to find all, we MUST mention all categories here.
        Categories.free;
        debugln('We have metadata = ' + inttostr(Ex.Count));
        if Ex.ErrorMsg <> '' then begin
            showmessage(Ex.ErrorMsg);
            exit;
        end;
        if Ex.GetListData(Proj, Cat, Path, Keys, true) then begin
            CheckTheSpelling(Ex.GetDesc(Path+Proj), Path+Proj);
            while Ex.GetListData(Proj, Cat, Path, Keys, false) do
                CheckTheSpelling(Ex.GetDesc(Path+Proj), Path+Proj);
        end;
    finally
        Ex.Free;
        DebSpell.Free;
    end;
    showmessage('Detected ' + inttostr(Cnt) + ' bad spellings');
end;

// curl "https://gitlab.com/api/v4/projects/32866275/repository/files/Utility%2FExScanner%2Fproject1.ico?ref=main"


// ------------------------------ House Keeping --------------------------------

procedure TForm2.FormCreate(Sender: TObject);
begin
    SetSomeDefaults();
    SRCFiles := nil;
end;

procedure TForm2.FormDestroy(Sender: TObject);
begin
    FreeAndNil(SRCFiles);
end;

procedure TForm2.FormResize(Sender: TObject);
var
    W : integer;
begin
    W := (Panel1.Width div 2) - 20;
    EditLazConfigDir.Width := W;
    EditLocalGitDir.width := W;
end;

procedure TForm2.FormShow(Sender: TObject);
begin
    EditJName.SetFocus;
end;



procedure TForm2.SetSomeDefaults();
begin
    // ToDo : these defaults will NOT suit anyone other than me, need an config file .....

    {$ifdef UNIX}
    Home := GetEnvironmentVariable('HOME');
    EditLazConfigDir.Text := Home + DEFAULT_LAZ_CONFIG;             // Where we save downloaded/copied projects

    EditStandAloneGit.Text := Home + DEFAULT_LAZ_UP_GIT;            // A different git dir, upload to make on line repo


    EditLocalGitDir.Text := Home + DEFAULT_LAZ_DOWN_GIT;
            // Set starts here, and we save the metadata whereever Set is set to.
    {$endif}
    {$ifdef WINDOWS}              // This app is getting less and less likely on Windows/Mac ....
    Home := GetEnvironmentVariable('HOMEPATH');
    EditLazConfigDir.Text := Home + '\AppData\Local\lazarus\';
    EditStandAloneGit.Text := 'c:\lazarus\';
    EditLocalGitDir.Text := 'c:\Lazarus\lazarus-main\';         // thats where mine is ......
    {$endif}

    EditRemoteRepo.Text := DEFAULT_REMOTE_EXAMPLES;

    // some things we may not be using in current manifestation, legacy ??
    // EditRemoteRepo.Enabled := false;
    // EditLazConfigDir.Enabled := false;
    //GroupBox1.enabled := false;
    //ButtonUserWindow.enabled := false;
    ButtonEditMeta.Enabled := false;



    ComboJCategory.Items.Clear;
    ComboJCategory.Items.Add('Beginner');
    ComboJCategory.Items.Add('General');
    ComboJCategory.Items.Add('DBase');
    ComboJCategory.Items.Add('OPM');
    ComboJCategory.Items.Add('TAChart');
    ComboJCategory.Items.Add('LazReport');
    EditJName.Text := GETTINGSTARTED;
    EditJkeyWords.Text := '';
end;


end.

