program project1;

{$mode objfpc}{$H+}

uses
    {$IFDEF UNIX}
    cthreads,
    {$ENDIF}
    {$IFDEF HASAMIGA}
    athreads,
    {$ENDIF}
    Interfaces, // this includes the LCL widgetset
    Forms, lazcontrols, uexampledata, uLaz_Examples, Main_Examples,
    uChangeStuff, uDebSpell, udump;

{$R *.res}

begin
    RequireDerivedFormResource := True;
    Application.Scaled := True;
    Application.Initialize;
    Application.CreateForm(TForm2, Form2);
    Application.CreateForm(TFormLazExam, FormLazExam);
    Application.CreateForm(TFormChangeStuff, FormChangeStuff);
    Application.CreateForm(TFormDump, FormDump);
    Application.Run;
end.

