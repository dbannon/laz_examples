unit udump;

{$mode ObjFPC}{$H+}

interface

uses
    Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

    { TFormDump }

    TFormDump = class(TForm)
        Memo1: TMemo;
        procedure FormShow(Sender: TObject);
    private
        procedure Dump;

    public
        InString : string;

    end;

var
    FormDump: TFormDump;

implementation

{$R *.lfm}

{ TFormDump }

procedure TFormDump.FormShow(Sender: TObject);
begin
    Dump;
end;

const TenSpace='          ';

procedure TFormDump.Dump;
var
    i : integer;
    St :  string = '';
begin
    Memo1.Clear;
    for i := 1 to length(InString) do
        if (InString[i] > ' ') and (InString[i] <= '~') then
            St := St + InString[i]
        else begin
            Memo1.Append(St);           // one word at a time.
            St := '';
            case ord(InString[i]) of
               10  : Memo1.Append(TenSpace + 'Line Feed');
               13  : Memo1.Append(TenSpace + 'Carriage Ret');
                32 : Memo1.Append(TenSpace + 'Space');
            else
                Memo1.Append(TenSpace + 'Ord ' + inttostr(ord(InString[i])));
            end;
        end;
   if St <> '' then Memo1.Append(St);
end;

end.

